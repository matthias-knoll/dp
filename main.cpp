#include "parser.h"

// 07.04.2018: 0.5 - complete refactoring of parser(...), the first working prototype
// 11.04.2018: 0.6 - improvements, better error handling, removing useless babble to stderr, adding functions like 
/*	warn levels
	named columns:
	columnname(<file>,"name1,name2,nameN,...")
	refactoring of csvjoin and column limits into library functions
	numberop(): number operations - things like [columnname]>=[number], [columnname]<=[number] etc.

*/
/*
10/2018: 0.7 - see parserlib.cpp for comments and updates

18.06.2018: 0.8 -  
				sum_fs(): 4 th argument for setprecision()
				
				12/2019:
				fixed bug in numericalsort: duplicate keys are not discarded, column order is maintained
				new functions: insert, append, insertdata, appenddata, fs_usage
				better help function
				syntax function for syntax errors
				function calls on commandline are now supported
				
	ld -r -b binary -o dp_man.o dp_man.txt
	ld -r -b binary -o syntax.o syntax.txt
	g++ -o dp2 main.cpp parserlib.cpp lib.cpp libfunctions.cpp dp_man.o syntax.o -O3 -std=c++0x

19.12.2019: 0.9 
		migrating to function pointers

08.03.2022:
  using double in find_number function
  fix sum() function
  
*/

string version="0.91 build 007, Mar 10, 2022";

extern char _binary_dp_man_txt_start;
extern char _binary_dp_man_txt_end;

void help() {
	char*  p = &_binary_dp_man_txt_start;
    while ( p != &_binary_dp_man_txt_end ) putchar(*p++);
	exit(0);
}

void usage(string p) {
	cerr << p << " <script> [args,...] [--help]\n";
	cerr << p << " --help: shows all functions\n";
	cerr << p << " --version\n";
	exit(0);
}

int main (int argc, char *argv[] ) {

	char buf[BUFSIZ];
	vector<string>input;
	vector<string>variablemarker;
	vector<string>functionmarker;
	vector<string>args;
	map<string,int>functiontable; // name of function and number of arguments
	string s, script;
	map<string,int>::iterator it;
	if (argc < 2 ) usage("dp");

	script=argv[1];
	
	variablemarker.push_back("${");
	variablemarker.push_back("}");
	functionmarker.push_back("(");
	functionmarker.push_back(")");
	
	/* register functions - a function called not being here will result in a syntax error & quit. */
	functiontable["readfile"]=1; // get data from file
	functiontable["read_match"]=2; // get data from file but only store lines that match
	functiontable["setdata"]=1; // overwrite the current memory (vector)
	functiontable["command"]=1; //run commands, write errors to stdout
	functiontable["command2line"]=1; // get single line data from stdout, variable number of args
	functiontable["command2data"]=1; // get data vector from stdout, variable number of args
	functiontable["print"]=1;
	functiontable["stdout"]=0;	
	functiontable["order"]=1;
	functiontable["unique"]=0;
	functiontable["merge"]=2;
	functiontable["find_highest"]=2;
	functiontable["find_highest2"]=2;	
	functiontable["find_lowest"]=2;
	functiontable["sum"]=2;
	functiontable["sum_fs"]=4;
	functiontable["fs_usage"]=3;
	functiontable["writefile"]=1;
	functiontable["appendfile"]=1;
	functiontable["simplejoin"]=3;
	functiontable["numericalsort"]=2;
	functiontable["remove"]=1;
	functiontable["match"]=2;
	functiontable["match_column"]=3;
	functiontable["match_data"]=2;	
	functiontable["remove_empty"]=0;
	functiontable["exit"]=0;
	functiontable["count"]=0;
	functiontable["count_data"]=1;
	functiontable["timer_start"]=0;
	functiontable["timer_end"]=0;
	functiontable["showvariables"]=0;
	functiontable["tail"]=1;
	functiontable["head"]=1;
	functiontable["epoch"]=1;
	functiontable["test"]=1;
	functiontable["columns"]=1; 
	functiontable["sort"]=1;
	functiontable["setcsvseparator"]=1;
	functiontable["math"]=2;
	functiontable["grep"]=2;	
	functiontable["grep_column"]=3;	
	functiontable["writedata"]=2;	
	functiontable["appenddata"]=2;
	functiontable["insert"]=1;
	functiontable["append"]=1;
	
	if ( script == "--help") 
		help();
	
	if ( script == "--version") {		
		cout << version << endl;
		return 0;
	}
	
	for (auto i=1; i<argc; i++ ) args.push_back(string(argv[i]));
	
	parser::parser(script, functiontable, args);

return 0;
}