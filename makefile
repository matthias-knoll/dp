dp:  main.cpp parserlib.cpp lib.cpp libfunctions.cpp dp_man.o syntax.o 
	clang++ -o dp2 main.cpp parserlib.cpp lib.cpp libfunctions.cpp dp_man.o syntax.o -O3 -std=c++20

syntax.o: syntax.txt
	ld -r -b binary -o syntax.o syntax.txt

dp_man.o: dp_man.txt
	ld -r -b binary -o dp_man.o dp_man.txt

install: dp2
	sudo install -d /usr/local/bin
	sudo install -m 755 dp2 /usr/local/bin