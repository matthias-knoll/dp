#include "parser.h"

using namespace std;
using namespace lib;

// 03.04.2018: 0.1
// 04.04.2018: 0.2
// 05.04.2018: 0.3
// 06.04.2018: 0.4 assign return values to variables
// note: variables that will store return values should not be predeclared.
// variable values must be quoted when declared.
// declared variables are const!
// 02.10.2018: 0.7 new functions:
/*				- find_highest uses find_number_column(..)
                                - math()
        06.10.2018:
                                - added datamanagement: several blocks of data
   (vector<string>) can now be stored into variables new functions that return
   data: grep() grep_column()

                                setdata() copies the data into the main vector
   that gets processed. 19.06.2019: 0.8
                                - accepting arguments (argv[n])
                                - merge() - only characters to concat are
   allowed
                                - functionparts() use rfind (find would truncate
   args if ) are in string literals)
*/
namespace parser {

//-------------------------------------------
// globals
bool dbg = false;
bool WARN = false;
bool INFO = true;

char COMMENT = '#';
char CSVDELIM = ',';
char argdelim = ',';

vector<string> UNITS = {"mb", "b", "kb", "tb", "gb"};
vector<string> DATA;
string SCRIPT;

string assignpattern = "<-";
string var_start = "${";
string var_end = "}=";
string func_start = "(";
string func_end = ")";

string message = "";

string action;

// not sure if this is not causing problems
vector<string> function_args;
string function_name = "";
string variable_name = "";
string variable_value = "";
string returnvariable_name = "";
string returnvariable_value = "";

const int FATAL = 100;

string endmarker = ";";

double timer_start, timer_end;

map<string, int> functiontable;
map<string, vector<string>> functions;
map<string, int> original_lines;
map<string, string> variables;
map<string, vector<string>> datastore;
vector<string> args; // arguments given on commandline, i.e. when dp is called

// a map per csv file that stores the column position (index) of named columns,
// which can be used later when spliting each line  - i.e in simplejoin or order
// i.e a csvfile has 3 columns: host,workflow,status then host has index 0,
// workflow index 1 and status has index 2
map<string, map<string, int>> columnnames;

// store function pointers
map<string, int (*)(vector<string>)> _functions;
map<string, string (*)(vector<string>)> _rfunctions;
map<string, vector<string> (*)(vector<string>)> _dfunctions;

//-------------------------------------------
// functions
//-------------------------------------------

void syntax() {
  char *p = &_binary_syntax_txt_start;
  while (p != &_binary_syntax_txt_end)
    putchar(*p++);
  exit(20);
}

void setdata(vector<string> &v, string arg) {

  if (datastore[arg].size() == 0) {
    cerr << "error 	setdata(): " << arg << " is empty.";
    exit(31);
  }
  v = datastore[arg];
}

void setcolumnnames(vector<string> args) {

  if (args.size() < 2) {
    cerr << "error 	setcolumnnames(): not enough arguments, at least 2 "
            "needed.";
    exit(30);
  }

  string filename = args[0];
  map<string, int> names;
  int pos = 0;
  for (auto i = 1; i < args.size(); i++) {
    trim(args[i]);
    names[args[i]] = pos++;
  }
  columnnames[filename] = names;
}

int fs_usage(vector<string> &v, string data_columns, string unit,
             string precision) {

  long ln = 0, cnt = 0;
  double n, sz, sum_gb = 0, sum_tb = 0, d_total = 0, d_used = 0;
  string used, total, size, name, str, s_used, s_total;
  char buf[BUFSIZ];
  int pos;
  int mult = 1024;
  int kb = 0, mb = 0, gb = 0, tb = 0, b = 0, pr;
  int baseunit_index = -1;

  stringstream ss;

  map<string, double> m_sum;
  map<string, double>::iterator it;
  vector<string> store;

  vector<int> p;
  trim(precision);
  trim(unit);
  transform(unit.begin(), unit.end(), unit.begin(), ::tolower);

  for (auto i = 0; i < UNITS.size(); i++) {
    if (UNITS[i] == unit)
      baseunit_index = i;
  }

  if (baseunit_index == -1) {
    cerr << "error - fs_usage(): invalid second argument (" << unit
         << "). Use: B,KB,MB,GB or TB\n";
    exit(1);
  }

  if (dbg)
    cerr << "baseunit_index:" << baseunit_index << "(" << UNITS[baseunit_index]
         << ")\n";

  try {
    pr = stoi(precision);
  } catch (std::invalid_argument &e) {
    cerr << "error: sum_fs(): invalid 4th argument for precision:" << precision
         << "\n";
    exit(2);
  }

  ss.setf(ios::fixed);

  if (!parser::string2index(p, data_columns)) {
    cerr << "error - fs_usage() - wrong first argument:" << data_columns
         << endl;
    exit(3);
  }

  if (p.size() > 3) {
    cerr << "warn - fs_usage(): only first 3 arguments are used, " << p.size()
         << " were provided.\n";
  }

  // adding / counting
  for (auto i = 0; i < v.size(); i++) {
    string currentline = v[i];
    vector<string> data = csvsplit(currentline, CSVDELIM);
    int ln = i;

    if (data.size() < 3) {
      cerr << "error fs_usage() - invalid line (" << ln++ << "|" << data.size()
           << "):" << v[i] << "\n";
      continue;
    }
    for (auto j = 0; j < p.size(); j++) {
      if (p[j] >= data.size()) {
        cerr << "error fs_usage() - column out of range:" << p[j] + 1
             << " (first arg:" << data_columns << ")\n";
        exit(1);
      }
    }
    name = data[p[0]];
    used = data[p[1]];
    total = data[p[2]];

    transform(total.begin(), total.end(), total.begin(), ::toupper);
    transform(used.begin(), used.end(), used.begin(), ::toupper);
    int t_matched_unit_index,
        u_matched_unit_index; // size position that matched within UNITS

    d_used = parser::convert_size(used, UNITS, 0);
    d_total = parser::convert_size(total, UNITS, 0);

    if (d_total == -1) {
      cerr << "error calculating total size in line " << (i + 1) << ", column "
           << p[2] + 1 << ": " << total << endl;
      continue;
    }

    if (d_used == -1) {
      cerr << "error calculating used size in line " << (i + 1) << ", column "
           << p[1] + 1 << ": " << used << endl;
      continue;
    }

    ss << name << CSVDELIM << setprecision(pr) << (d_used / d_total) * 100;
    store.push_back(ss.str());
    ss.clear();
    ss.str("");
  }
  v = store;

  return 0;
}

string extract_size(string s, vector<string> units, int &matchedunit_index) {

  transform(s.begin(), s.end(), s.begin(), ::tolower);
  size_t p, p1;
  int dotcnt = 0, start, found = 0;
  string unit, num = "";
  char c, size_separator = ' ';
  bool b = false;

  trim(s);

  // need to insert size_separator in case unit and size are not separated
  if (s.find(size_separator) == string::npos) {
    string o = "";
    for (auto i = 0; i < s.length(); i++) {
      c = s[i];
      if (isalpha(c) && !b) {
        o += size_separator;
        b = true;
      }
      o += c;
    }
    s = o;
  }

  vector<string> words = ssplit(s, size_separator);

  if (dbg) {
    cerr << "dbg - extract_size(): words vector:" << endl;
    vp(words);
  }

  for (auto i = 0; i < units.size(); i++) {
    unit = units[i];
    transform(unit.begin(), unit.end(), unit.begin(), ::tolower);
    for (auto j = 0; j < words.size(); j++) {
      trim(words[j]);
      if (words[j] == "") {
        if (dbg)
          cerr << "dbg - extract_size() skip empty vector element in words ("
               << j << ")\n";
        continue;
      }

      // one word
      if (words.size() == 1) {
        p1 = s.rfind(unit);
        if (p1 != string::npos) {
          p = p1;
          found++;
        }
        if (dbg)
          cerr << "dbg - extract_size() unit: " << unit << "|" << words[j]
               << " found:" << found << " " << p << endl;
      }

      if (unit == words[j]) {
        if (dbg)
          cerr << "dbg - extract_size() match unit: " << unit << "|" << words[j]
               << endl;
        matchedunit_index = i;
        if (!found)
          p = s.rfind(unit);
        found++;
      }
    }
  }

  if (found != 1)
    return num; // must find only one valid size measure

  s = s.substr(0, p);

  if (dbg)
    cerr << "dbg - extract_size() substr: " << s << " pos:" << p << endl;

  for (start = (p - 1); start >= 0; start--) {
    c = s[start];
    if (c == ' ' && num.length() == 0)
      continue;

    if (dbg)
      cerr << "dbg - extract_size():" << start << " " << c << endl;

    if (c == '.' || c == ',') {
      if (dotcnt == 0)
        num += c;
      dotcnt++;
      continue;
    }

    if (isalpha(c) || isspace(c))
      break;

    if (isdigit(c) || c == '.' || c == ',')
      num += c;
  }
  rtrim(num, ',');
  rtrim(num, '.');
  srev(num);
  return num;
}

// sums up data size - units are: b,kb,mb,gb,tb
void sum_fs(vector<string> &v, string data_columns, string column, string unit,
            string precision) {

  long ln = 0, cnt = 0;
  double n, sz, sum_gb = 0, sum_tb = 0;
  string size, str, num;
  char buf[BUFSIZ];
  int pos;
  int mult = 1024;
  int kb = 0, mb = 0, gb = 0, tb = 0, b = 0, pr;

  stringstream ss;

  map<string, double> m_sum;
  map<string, double>::iterator it;
  vector<string> store;

  vector<int> p;
  vector<int> c;
  trim(precision);
  try {
    pr = stoi(precision);
  } catch (std::invalid_argument &e) {
    cerr << "error: sum_fs(): invalid 4th argument for precision:" << precision
         << "\n";
    exit(21);
  }

  ss.setf(ios::fixed);

  if (!parser::string2index(p, data_columns)) {
    cerr << "error - sum_fs() - wrong first argument:" << data_columns << endl;
    return;
  }

  if (!parser::string2index(c, column)) {
    cerr << "error - sum_fs() - wrong second argument:" << column << endl;
    return;
  }

  // adding / counting
  for (auto i = 0; i < v.size(); i++) {
    string currentline = v[i];
    vector<string> data = csvsplit(currentline, CSVDELIM);

    if (c[0] >= data.size()) {
      
      cerr << "error: position of out range (" << data.size() << "):" << c[0]
           << "\n"
           << "line " << i << ":" << currentline << endl;
      
      continue;
    }

    size = data[c[0]];
    string key = "";
    if (buildstring(p, data, key, CSVDELIM))
      continue;

    transform(size.begin(), size.end(), size.begin(), ::toupper);
    transform(unit.begin(), unit.end(), unit.begin(), ::toupper);
    int matched_unit_index; // size position that matched within UNITS
    num = parser::extract_size(size, UNITS, matched_unit_index);

    if (num == "") {
      cerr << "error invalid line (" << (i + 1) << "):" << trim(size) << endl;
      continue;
    }

    n = s_todouble(num);

    if (n == -1) {
      cerr << "error calculating size in line " << (i + 1) << " - value:" << num
           << endl;
    }

    if (UNITS[matched_unit_index] == "b") {
      m_sum[key] += (n / (1024 * 1024));
    }

    if (UNITS[matched_unit_index] == "kb") {
      m_sum[key] += (n / 1024);
    }

    if (UNITS[matched_unit_index] == "mb") {
      m_sum[key] += n;
    }

    if (UNITS[matched_unit_index] == "gb") {
      m_sum[key] += (n * 1024);
    }

    if (UNITS[matched_unit_index] == "tb") {
      m_sum[key] += (n * (1024 * 1024));
    }
  }

  for (it = m_sum.begin(); it != m_sum.end(); it++) {
    double size_converted = 0;

    if (unit == "B")
      size_converted = it->second * (1024 * 1024);
    if (unit == "KB")
      size_converted = it->second * 1024;
    if (unit == "MB")
      size_converted = it->second;
    if (unit == "GB")
      size_converted = (it->second / 1024);
    if (unit == "TB")
      size_converted = (it->second / (1024 * 1024));

    ss << it->first << CSVDELIM << setprecision(pr) << size_converted << " "
       << unit;

    store.push_back(ss.str());
    ss.clear();
    ss.str("");
  }
  v = store;

  return;
}

int evalvariables(string &line, map<string, string> var) {
  int n = 0, replaced = 0;
  map<string, string>::iterator it;
  string func = "evalvariables()";

  vector<string> val;
  vector<string> key;

  // not a variable declaration or assignment from a function
  if (line.find(var_start) == string::npos ||
      line.find(assignpattern) != string::npos) {
    // line="";
    if (dbg)
      cerr << "dbg - " << func << ": nothing to do (" << var_start
           << " not found or assignpattern " << assignpattern
           << " found) line:" << line << endl;
    return 0; // nothing to do
  }

  // add variables
  for (it = var.begin(); it != var.end(); it++) {
    if (string_replace(line, it->first, it->second))
      replaced++;

    if (dbg)
      cerr << "dbg - " << func << " var:" << it->first << " val:" << it->second
           << endl;
  }

  if (dbg)
    cerr << "dbg - " << func << ": transformed line:" << line << endl
         << replaced << " variables replaced\n";

  return replaced;
}

void setcsvseparator(string arg) {
  trim(arg);
  CSVDELIM = arg[0];
  if (INFO)
    cerr << "info - delimiter is set to:" << CSVDELIM << endl;
}

void setcomment(char c) { COMMENT = c; };
// example (script): find_highest("1","2")

void find_number(vector<string> &v, string pattern, string column,
                 string type) {

  vector<int> p; // positions
  vector<int> c; // positions
  vector<string> store;
  int pos;
  int col;
  trim(pattern);
  trim(type);
  map<string, double> m;
  map<string, string> result;
  map<string, string>::iterator it;
  map<string, double>::iterator itn;

  if (dbg)
    cerr << "dbg - call find_number(" << pattern << "," << column << ")\n";

  // get vector indices
  if (!parser::string2index(p, pattern)) {
    cerr << "error - find_number() - wrong first argument:" << pattern << endl;
    return;
  }

  // set column
  if (!parser::string2index(c, column)) {
    cerr << "error - find_number() - wrong second argument:" << column << endl;
    return;
  }

  col = c[0];

  // go through data
  for (auto i = 0; i < v.size(); i++) {

    string search = "", tmp = "";
    bool skip = false;
    vector<string> o = ssplit(v[i], CSVDELIM);
    
    if (v[i].empty() ) continue;

    for (auto n = 0; n < p.size(); n++) {
      if (p[n] >= o.size()) {
        cerr << "error find_number(" << type << "): position of out range ("
             << o.size() << "):" << p[n] << "line " << i << ":" << v[i] << endl;
        return;
      }
      search += o[p[n]];
      if (n < p.size() - 1)
        search += CSVDELIM;
    }

    if (dbg)
      cerr << "    dbg - find_number() [" << i << "] search " << search << endl;

    if (v[i].find(search) == string::npos || col >= o.size())
      continue; // nothing found or not enough columns

    if (dbg)
      cerr << "    dbg - find_number() pattern " << search << " found\n";

    double number = s_todouble(o[col]);

    if (number == -1) {
      if (dbg)
        cerr << "    dbg - find_number() column:" << (col + 1)
             << " - not a number " << number << " (" << o[col] << ")\n";
      continue;
    }

    if (dbg)
      cerr << "    dbg - find_number() pattern:" << search
           << " number:" << number << " (" << o[col] << ")\n";

    // data not set, initial value
    if (m[search] == 0) {
      if (dbg)
        cerr << "    dbg - find_number() set m[search]:" << m[search] << "="
             << number << endl;
      m[search] = number;
      result[search] = v[i];
      continue;
    }

    if (type == "lowest") {
      // update map
      if (number < m[search]) {
        m[search] = number;
        result[search] = v[i];
        if (dbg)
          cerr << "    dbg - find_number_lowest() update key(" << search << ") "
               << v[i] << "\n";
      }
      continue;
    }

    string msg = "";
    if (result[search].empty())
      msg = "add";
    else
      msg = "update";

    if (type == "highest") {
      if (number > m[search]) {
        m[search] = number;
        result[search] = v[i];
        if (dbg)
          cerr << "    dbg - find_number() " << msg << " key(" << search << ") "
               << v[i] << "\n";
      }
      continue;
    }
    if (type == "sum") {
      m[search] += number;
      continue;
    }
  }

  if (dbg)
    cerr << "    dbg - find_number() change vector size:" << v.size();

  v.clear();

  if (type == "sum") {
    for (itn = m.begin(); itn != m.end(); itn++) {
      stringstream ss;
      ss << itn->first << CSVDELIM << itn->second;
      v.push_back(ss.str());
      ss.clear();
      ss.str("");
    }
    return;
  }

  for (it = result.begin(); it != result.end(); it++) {
    v.push_back(it->second);
  }
  if (dbg)
    cerr << " to " << v.size() << endl;
}

void find_number_lowest(vector<string> &v, string pattern, string column) {

  vector<int> p; // positions
  vector<int> c; // positions
  vector<string> store;
  int pos;
  int col;
  trim(pattern);
  map<string, long> m;
  map<string, string> result;
  map<string, string>::iterator it;
  map<string, long>::iterator itn;

  if (dbg)
    cerr << "dbg - call find_number(" << pattern << "," << column << ")\n";

  // get vector indices
  if (!parser::string2index(p, pattern)) {
    cerr << "error - find_number() - wrong first argument:" << pattern << endl;
    return;
  }

  // set column
  if (!parser::string2index(c, column)) {
    cerr << "error - find_number() - wrong second argument:" << column << endl;
    return;
  }

  col = c[0];

  // go through data
  for (auto i = 0; i < v.size(); i++) {

    string search = "", tmp = "";
    bool skip = false;
    vector<string> o = ssplit(v[i], CSVDELIM);

    for (auto n = 0; n < p.size(); n++) {
      if (p[n] >= o.size()) {
        cerr << "error find_number_lowest(): position of out range ("
             << o.size() << "):" << p[n] << "line " << i << ":" << v[i] << endl;
        return;
      }
      search += o[p[n]];
      if (n < p.size() - 1)
        search += CSVDELIM;
    }

    if (dbg)
      cerr << "    dbg - find_number_lowest() [" << i << "] search " << search
           << endl;

    if (v[i].find(search) == string::npos || col >= o.size())
      continue; // nothing found or not enough columns

    if (dbg)
      cerr << "    dbg - find_number_lowest() pattern " << search << " found\n";

    long number = s_tolong(o[col], true);

    if (number == -1) {
      if (dbg)
        cerr << "    dbg - find_number_lowest() column:" << col
             << " - not a number " << number << " (" << o[col] << ")\n";
      continue;
    }

    if (dbg)
      cerr << "    dbg - find_number_lowest() " << i << " pattern:" << search
           << " number:" << number << "  m[search]:" << m[search] << " ("
           << o[col] << ")\n";

    string msg = "";
    // m[search]=number;

    // result[search]=v[i];
    if (m[search] == 0) {
      if (dbg)
        cerr << "    dbg - find_number_lowest() set m[search]:" << m[search]
             << "=" << number << endl;
      m[search] = number;
      result[search] = v[i];
      continue;
    }

    if (number < m[search]) {
      m[search] = number;
      result[search] = v[i];
      if (dbg)
        cerr << "    dbg - find_number_lowest() update key(" << search << ") "
             << v[i] << "\n";
    }
  }

  if (dbg)
    cerr << "    dbg - find_number_lowest() change vector size:" << v.size();

  v.clear();
  for (it = result.begin(); it != result.end(); it++) {
    v.push_back(it->second);
  }
  if (dbg)
    cerr << " to " << v.size() << endl;
}

void find_number_column(vector<string> &v, string pattern_column,
                        string search_column, string type) {

  vector<int> p; // positions
  vector<int> c; // positions
  vector<string> store;
  int pos;
  int col;
  trim(pattern_column);
  map<string, long> m;
  map<string, string> result;
  map<string, string>::iterator it;
  map<string, long>::iterator itn;

  if (dbg)
    cerr << "dbg - call find_number_column(" << pattern_column << ","
         << search_column << ")\n";

  // get vector indices
  col = s_toint(pattern_column);
  pos = s_toint(search_column);
  col--;
  pos--;

  if (col < 0) {
    cerr << "error - find_number_column(" << type
         << ") - wrong first argument:" << pattern_column << endl;
    exit(10);
  }

  // set column
  if (pos < 0) {
    cerr << "error - find_number_column(" << type
         << ") - wrong second argument:" << search_column << endl;
    exit(10);
  }

  // go through data
  for (auto i = 0; i < v.size(); i++) {

    string search = "", tmp = "";
    bool skip = false;
    vector<string> o = ssplit(v[i], CSVDELIM);
    search = o[col];
    long number = s_tolong(o[pos], true);

    if (pos >= o.size()) {
      cerr << "error find_number_column(" << type
           << "): search position of out range (" << o.size() << "):" << pos
           << endl;
      continue;
    }

    if (col >= o.size()) {
      cerr << "error find_number_column(" << type
           << "): key position of out range (" << o.size() << "):" << col
           << endl;
      continue;
    }

    if (number == -1) {
      if (dbg)
        cerr << "    dbg - find_number_column():" << o[pos]
             << " - not a number " << number << " (" << o[col] << ")\n";
      continue;
    }

    if (type == "sum") {
      m[search] += number;
      continue;
    }

    string msg = "";

    if (result[search].empty())
      msg = "add";
    else
      msg = "update";

    m[search] = number;

    if (type == "highest") {
      if (number > m[search]) {
        m[search] = number;
        result[search] = v[i];
        // cerr << "find_number_column() " << msg << " key(" << search << ") "
        // << m[search] <<"\n"; cerr << "   result:" << result[search] << endl;
      }
    }

    // result[search]=v[i];

    if (type == "lowest") {
      if (number < m[search] && m[search] > 0) {
        m[search] = number;
        result[search] = v[i];
        if (dbg)
          cerr << "    dbg - find_number_column() " << msg << " key(" << search
               << ") " << v[i] << "\n";
      }
    }
  }

  if (dbg)
    cerr << "    dbg - find_number_column() change vector size:" << v.size();

  v.clear();

  if (type == "sum") {
    for (itn = m.begin(); itn != m.end(); itn++) {
      stringstream ss;
      ss << itn->first << CSVDELIM << itn->second;
      v.push_back(ss.str());
      ss.clear();
      ss.str("");
    }
    return;
  }

  // write back result to vector
  for (it = result.begin(); it != result.end(); it++) {
    v.push_back(it->second);
  }
  if (dbg)
    cerr << " to " << v.size() << endl;
}

// isolates numbers from a string, skips punctuation, i.e is
// not detecting float
long string_tolong(string s) {
  string r = "";
  long n = -1;
  char c;
  lib::trim(s);
  if (s.empty())
    return -1;

  for (auto i = 0; i < s.length(); i++) {
    c = s[i];
    if (isdigit(c))
      r += c;
    if (isalpha(c) && r.length() > 0)
      continue;
  }

  if (r != "") {
    try {
      n = stol(r);
    } catch (std::invalid_argument &e) {
      cerr << "error: string_tolong() invalid non-numeric argument:" << s
           << "\n";
      return -1;
    }
  }

  return n;
}

void math(vector<string> &v, string column, string arg) {

  vector<int> p; // positions
  vector<int> c; // positions
  vector<string> store;
  vector<string> valid_operations = {"largerthan", "greaterthan", "morethan",
                                     "lessthan", "equals"};
  int pos;
  int col;
  trim(column);
  string type = "", operation = "";
  long value = 0;
  bool done = false;

  // get vector indices
  col = s_toint(column);
  col--;

  if (col < 0) {
    cerr << "error - math() - wrong first argument:" << column << endl;
    return;
  }

  vector<string> av = ssplit(arg, " ");
  value = s_tolong(av.back(), true);
  av.pop_back();

  for (auto i = 0; i < av.size(); i++) {
    trim(av[i]);
    operation += av[i];
  }
  transform(operation.begin(), operation.end(), operation.begin(), ::tolower);
  trim(operation);

  if (std::find(valid_operations.begin(), valid_operations.end(), operation) ==
      valid_operations.end()) {
    cerr << "error - math() - invalid operation:" << arg << endl;
    exit(10);
  }

  // go through data
  for (auto i = 0; i < v.size(); i++) {

    done = false;
    vector<string> o = ssplit(v[i], CSVDELIM);
    if (col >= o.size()) {
      cerr << "error - math(): data only has only " << o.size() << " columns"
           << endl;
      exit(10);
    }

    long number = s_tolong(o[col], true);

    if (number == -1)
      continue;

    if (operation == "largerthan" && number > value) {
      store.push_back(v[i]);
      done = true;
    }

    // same as larger ..
    if (operation == "greaterthan" && number > value) {
      store.push_back(v[i]);
      done = true;
    }

    // same as larger ..
    if (operation == "morethan" && number > value) {
      store.push_back(v[i]);
      done = true;
    }

    if (operation == "lessthan" && number < value) {
      store.push_back(v[i]);
      done = true;
    }

    if (operation == "equals" && number == value) {
      store.push_back(v[i]);
      done = true;
    }
  }

  v.clear();
  v = store;
}

bool extr_s(string in, string &result, string pattern, string endmarker) {
  string s;
  size_t start, end;

  start = in.find(pattern);
  if (start == string::npos)
    return false;

  // extract number from the specified field
  s = in.substr(start);
  end = s.find(endmarker);
  if (end == string::npos)
    return false;
  int l = pattern.length() + 1; // pattern and colon
  result = s.substr(0 + l, end - l);
  trim(result);

  return true;
}

// extracts the key-value pair
bool extr_p(string in, string &result, string pattern, string endmarker) {
  string s;
  size_t start, end;

  start = in.find(pattern);
  if (start == string::npos)
    return false;

  // extract number from the specified field
  s = in.substr(start);
  end = s.find(endmarker);
  if (end == string::npos)
    return false;
  result = s.substr(0, end);
  // trim(result);

  return true;
}

void numberop(vector<string> &v, string op) {

  vector<int> c; // positions
  vector<string> store;
  int col;
  size_t pos;
  lib::trim(op);
  map<string, long> m;
  map<string, string> result;
  map<string, string>::iterator it;
  map<string, long>::iterator itn;
  char delim = ';';

  long op_number, number = -1;
  bool equals = false, less = false, greater = false;
  string pattern;
  size_t start, end;

  op_number = string_tolong(op);

  if (dbg)
    cerr << "dbg - call numberop(" << op << ")\n";

  // operation
  if (op.find("==") != string::npos) {
    equals = true;
    pattern = op;
    lib::sreplace(pattern, "==", ": ");
  }

  if (op.find(">=") != string::npos) {
    greater = true;
    start = op.find(">=");
    pattern = op.substr(0, start);
  }

  if (op.find("<=") != string::npos) {
    less = true;
    start = op.find("<=");
    pattern = op.substr(0, start);
  }

  cout << pattern << endl;

  if (pos == string::npos) {
    cerr << "error numberop() wrong argument" << op << endl;
    exit(20);
  }

  // go through data
  for (auto i = 0; i < v.size(); i++) {
    start = v[i].find(pattern);
    if (start == string::npos)
      continue;
    if (equals) {
      store.push_back(v[i]);
      continue;
    }

    // extract number from the specified field
    string s = v[i].substr(start);
    end = s.find(";");
    if (end == string::npos)
      continue;
    long number = string_tolong(s.substr(0, end));

    if (number == -1) {
      continue;
    }
    if (number >= op_number && greater) {
      // cerr << v[i] << endl;
      store.push_back(v[i]);
    }

    if (number <= op_number && less) {
      // cerr << v[i] << endl;
      store.push_back(v[i]);
    }
  }
  v = store;
  return;
}

// simplejoin() relies on key/value pairs colon separated, multiple pairs on a
// line a colon separated this is the standard flattened (one record per line)
// output format by jobquery and nsradmin
void simplejoin(vector<string> &v, vector<string> args) {

  vector<int> p, c; // positions
  vector<string> store;
  string file = args[0];
  // CSVDELIM=';';
  dbg = false;
  string searchkey = "";
  // creates a search pattern from columns in <file>, can be one or more columns
  // i.e "2" or "1,3"
  string column_search = args[1];

  // in this column in <file> is the value we add if search pattern is found in
  // data vector
  string column_add = args[2];

  size_t start, end;

  int pos;
  int col;
  trim(column_search);
  vector<string> dat = parser::readfile(file);
  map<string, string> joindata;
  map<string, string>::iterator it;

  if (dbg)
    cerr << "dbg - call simplejoin(" << file << "|" << column_search << "|"
         << column_add << ")\n";
  if (dbg)
    cerr << "dbg - simplejoin(): joindata:" << joindata.size() << " records\n";

  if (dat.size() == 0) {
    cerr << "warn - simplejoin(): file " << file << " is empty! Exiting."
         << endl;
    exit(30);
  }

  // create map of data to be joined
  for (auto i = 0; i < dat.size(); i++) {
    trim(dat[i]);

    if (dat[i].empty())
      continue;

    string val = "", key = "";

    bool bval = extr_p(dat[i], val, column_add, endmarker);
    bool bkey = extr_p(dat[i], key, column_search, endmarker);

    if (!bval || !bkey)
      continue;

    trim(key);
    trim(val);

    if (joindata[key].empty()) { // new key
      joindata[key] = val;
      if (dbg)
        cerr << "dbg - simplejoin() added key:" << key << "->join value:" << val
             << "\n";
    } else {
      cerr << "error - simplejoin() key:" << key
           << " is already defined:" << joindata[key] << "\n";
      cerr << " duplicate values will result in indefined datasets! Exiting.\n";
      exit(30);
    }
  }

  if (dbg)
    cerr << "dbg - simplejoin() joindata.size():" << joindata.size() << "\n";

  //----------------------------------------------------------------
  // parse through data

  int position = -1;

  for (auto i = 0; i < v.size(); i++) {

    vector<string> o = ssplit(v[i], CSVDELIM);
    // find once the column of the search pattern
    if (position < 0) {
      for (auto j = 0; j < o.size(); j++) {
        if (o[j].find(column_search) != string::npos) {
          position = j;
          if (dbg)
            cerr << "line " << i << "|column_search:" << column_search
                 << "|position:" << position << endl;
          break;
        }
      }
    }
    string s = v[i] + joindata[o[position]];
    store.push_back(s);
  }

  if (dbg)
    cerr << "change vector size from " << v.size();
  v.clear();
  v = store;
  if (dbg)
    cerr << " to " << v.size() << endl;
}

void order(vector<string> &v, string args) {

  vector<int> p; // positions
  vector<string> store;
  int pos;
  // dbg=false;

  if (dbg)
    cerr << "dbg - call order(" << args << ")\n";

  // get vector indices
  if (!parser::string2index(p, args)) {
    cerr << "error - order() - wrong first argument:" << args << endl;
    return;
  }

  for (auto i = 0; i < args.size(); i++) {
    if (dbg)
      cerr << "args " << i << ":" << args[i] << endl;
  }
  for (auto i = 0; i < p.size(); i++) {
    if (dbg)
      cerr << "p " << i << ":" << p[i] << endl;
  }
  if (dbg)
    cerr << "dbg - order(): call string2index():" << p.size()
         << " position(s) found\n";

  // go through data
  for (auto i = 0; i < v.size(); i++) {
    string tmp = "";
    bool skip = false;
    if (dbg)
      cerr << "    dbg - order() reorder line " << v[i] << "\n";
    vector<string> o = ssplit(v[i], CSVDELIM);
    if (dbg)
      cerr << "    dbg - order() line " << i << " " << o.size() << "\n";

    for (auto n = 0; n < p.size(); n++) {
      if (p[n] >= o.size()) {
        if (WARN)
          cerr << "warn: order() arg position out of range (" << p[n]
               << "), inputstream has only " << o.size() << " columns\n";
        skip = true;
        continue;
      }
      tmp += o[p[n]];
      if (dbg)
        cerr << "    dbg - order() line " << i << " add: (" << p[n] << ")"
             << o[p[n]] << " to string " << tmp << "\n";
      if (n < p.size() - 1)
        tmp += CSVDELIM;
    }
    if (!skip) {
      if (dbg)
        cerr << "dbg - order() store:" << tmp << "\n";
      store.push_back(tmp);
    }
  }
  v = store;
}

// merge together first and second,  fourth and fifth column with an underscore
// example(script): merge("1,4", "_")

// merge together first and second column with an dash
// example(script): merge("1", "-")

void merge(vector<string> &v, string pair1, char delim_pair) {

  vector<int> p1; // positions
  vector<string> store;
  int pos;
  string s, s2;
  bool skip1, skip2;
  int cn = delim_pair;

  if (isblank(delim_pair) || cn == 0) {
    cerr << "error - merge() - wrong/invalid second argument. Only characters, "
            "but not whitespace are allowed."
         << endl;
    return;
  }

  if (dbg)
    cerr << "dbg - call merge(" << pair1 << "," << delim_pair << ")\n";

  // get vector indices of pair 1
  if (!parser::string2index(p1, pair1)) {
    cerr << "error - merge() - wrong first argument:" << pair1 << endl;
    return;
  }

  if (dbg) {
    cerr << "dbg - merge(): pair1:" << p1.size() << " position(s) found\n";
    for (int i = 0; i < p1.size(); i++)
      cerr << p1[i] << endl;
  }

  char separator;
  for (auto i = 0; i < v.size(); i++) {
    vector<string> o = ssplit(v[i], CSVDELIM);
    s = "";
    for (auto j = 0; j < o.size(); j++) {
      for (auto k = 0; k < p1.size(); k++) {
        if (p1[k] >= o.size()) {
          if (dbg) {
            cerr << "dbg - merge() error: position out of range: " << p1[k]
                 << " (we only have " << o.size() << " columns)" << endl;
            cerr << "      line:" << v[i] << endl;
          }
          continue;
        }
      }
      if (find(p1.begin(), p1.end(), j) != p1.end()) {
        if (dbg)
          cerr << "dbg - merge() " << j << ": merge position (" << o.size()
               << ")\n";
        separator = delim_pair;
      } else {
        if (dbg)
          cerr << "dbg - merge() " << j << ": normal position (" << o.size()
               << ")\n";
        separator = CSVDELIM;
      }
      s += o[j];
      if (j < o.size() - 1)
        s += separator;
    }
    store.push_back(s);
  }
  v = store;
}

// removes lines with empty values in it
int remove_empty(vector<string> &lines) {
  vector<string> store;
  for (auto i = 0; i < lines.size(); i++) {
    trim(lines[i]);
    char lc = lines[i].at(lines[i].length() - 1);
    string pt = "";
    pt += CSVDELIM + CSVDELIM;
    // pt+=CSVDELIM;

    if (lines[i][0] == CSVDELIM || lc == CSVDELIM ||
        lines[i].find(pt) != string::npos) {
      if (INFO)
        cerr << "info - remove_empty() - remove:" << lines[i] << "\n";
      continue;
    }
    store.push_back(lines[i]);
  }
  lines = store;
  return 0;
}

void print(vector<string> v) {
  if (variables.find(v[0]) != variables.end())
    cout << variables[v[0]] << endl;
  else
    cout << v[0] << endl;
}

struct notempty {
  bool operator()(const std::string &s) { return !s.size(); }
};

// simple comparison between two vectors, if the strings from the second vector
// matches we either keep ("match") or remove ("remove") the line from the first
// vector
void match_data(vector<string> &v, vector<string> vs, string action) {
  bool keep = false;
  vector<string> store;

  // remove empty elements
  vs.erase(std::remove_if(vs.begin(), vs.end(), notempty()), vs.end());

  // go through data
  for (auto i = 0; i < v.size(); i++) {
    keep = false;
    for (auto j = 0; j < vs.size() && !keep; j++) {
      if (v[i].find(vs[j]) != string::npos) {
        keep = true;
        break;
      }
    }
    if (keep && action == "match")
      store.push_back(v[i]);
    if (!keep && action == "remove")
      store.push_back(v[i]);
  }
  v.clear();
  v = store;
}

// removes trailing delimiters and and rows that do not match the column number
// i.e one,two,three is 3 columns
// example(script): columns("1", ",")
void columns(vector<string> &v, string args, char delim) {

  int col = s_toint(args);
  vector<string> store;
  store = v;

  if (col == -1) {
    cerr << "error columns(): invalid argument: " << args << endl;
    return;
  }

  for (auto i = 0; i < store.size(); i++) {
    trim(store[i]);
    rtrim(store[i], delim); // remove trailing delimiters
    size_t n = count(store[i].begin(), store[i].end(), delim);
    // cout << col << "|" << n << endl;
    if (n != (col - 1)) {
      if (dbg)
        cerr << "dbg - columns(): wrong column number: " << n
             << "!=" << (col - 1) << " remove:" << store[i] << endl;
      store[i] = "";
    }
  }

  store.erase(remove_if(store.begin(), store.end(), notempty()), store.end());

  int diff = v.size() - store.size();
  if (dbg)
    cerr << "dbg - columns(): removed elements:" << diff << endl;
  v = store;
}

void match(vector<string> &v, string arg, string action, string matchcase) {
  if (dbg)
    cerr << "match()" << endl;
  vector<string> store;

  if (matchcase != "false" && matchcase != "true") {
    cerr << "error - grep_column(): second argument must be either be true or "
            "false."
         << endl;
    exit(10);
  }

  trim(arg);

  for (auto i = 0; i < v.size(); i++) {
    size_t p;

    if (matchcase == "false") {
      string s = v[i];
      string a = arg;
      transform(s.begin(), s.end(), s.begin(), ::tolower);
      transform(a.begin(), a.end(), a.begin(), ::tolower);
      p = s.find(a);
    } else
      p = v[i].find(arg);

    if (p != string::npos) {
      if (dbg)
        cerr << "dbg - match(" << action << "): pattern " << arg
             << " found. Line:" << v[i] << endl;
      if (action == "remove")
        v[i] = "";
      if (action == "match")
        store.push_back(v[i]);
      continue;
    }
  }
  if (action == "remove") {
    sort(v.begin(), v.end());
    v.erase(std::remove_if(v.begin(), v.end(), notempty()), v.end());
  }

  if (action == "match") {
    v.clear();
    v = store;
  }
}

// store into vector
vector<string> grep(vector<string> &v, string arg, string matchcase) {
  if (dbg)
    cerr << "grep()" << endl;

  if (matchcase != "false" && matchcase != "true") {
    cerr << "error - grep_column(): second argument must be either be true or "
            "false."
         << endl;
    exit(10);
  }

  vector<string> store;
  for (auto i = 0; i < v.size(); i++) {
    size_t p;
    string s = v[i];
    string a = arg;

    if (matchcase == "false") {
      transform(s.begin(), s.end(), s.begin(), ::tolower);
      transform(a.begin(), a.end(), a.begin(), ::tolower);
    }

    p = s.find(a);

    if (p != string::npos) {
      store.push_back(v[i]);
      continue;
    }
  }
  return store;
}

vector<string> grep_column(vector<string> &v, string arg, string column,
                           string matchcase) {
  if (dbg)
    cerr << "grep_column - vp()" << endl;
  vector<string> store;
  string type = "", operation = "", s = "";

  trim(column);
  int col = s_toint(column);
  col--;

  if (matchcase == "false")
    transform(arg.begin(), arg.end(), arg.begin(), ::tolower);

  if (matchcase != "false" && matchcase != "true") {
    cerr << "error - grep_column(): third argument must be either be true or "
            "false."
         << endl;
    exit(10);
  }

  for (auto i = 0; i < v.size(); i++) {

    vector<string> o = ssplit(v[i], CSVDELIM);
    if (col >= o.size())
      continue;

    size_t p;
    if (matchcase == "false") {
      transform(o[col].begin(), o[col].end(), o[col].begin(), ::tolower);
    }

    p = o[col].find(arg);

    if (p != string::npos) {
      store.push_back(v[i]);
      continue;
    }
  }
  cerr << "grep_column:" << store.size() << " matches" << endl;
  return store;
}

void match_column(vector<string> &v, string arg, string column,
                  string matchcase) {
  if (dbg)
    cerr << "match_column - vp()" << endl;
  vector<string> store;
  trim(column);
  string type = "", operation = "";
  int col = s_toint(column);
  col--;
  string s;

  if (matchcase == "false")
    transform(arg.begin(), arg.end(), arg.begin(), ::tolower);

  if (matchcase != "false" && matchcase != "true") {
    cerr << "error - match_column(): third argument must be either be true or "
            "false."
         << endl;
    exit(10);
  }

  for (auto i = 0; i < v.size(); i++) {

    vector<string> o = ssplit(v[i], CSVDELIM);
    if (col >= o.size())
      continue;

    size_t p;
    if (matchcase == "false") {
      transform(o[col].begin(), o[col].end(), o[col].begin(), ::tolower);
    }

    p = o[col].find(arg);

    if (p != string::npos) {
      store.push_back(v[i]);
      continue;
    }
  }

  v.clear();
  v = store;
}

void showvariables() {

  map<string, string>::iterator itv;
  ;
  map<string, vector<string>>::iterator itf;

  cout << "--------------------------------------\n";
  cout << "variables:\n";

  for (itv = variables.begin(); itv != variables.end(); itv++) {
    cerr << itv->first << "=" << itv->second << endl;
  }

  cout << "--------------------------------------\n";
  cout << "functions:\n";
  for (itf = functions.begin(); itf != functions.end(); itf++) {
    string args = "";
    if (!buildstring(itf->second, args, ','))
      continue;

    cerr << itf->first << " args:" << args << ")\n";
  }
}

bool isvariable(string line) {

  string f = "isvariable()";

  char lc = line.at(line.length() - 1);
  size_t start = line.find(var_start);
  size_t end = line.find(var_end);

  action = "call " + f;
  message = action + " - line:" + line + "\n";

  if (line.find("=") == string::npos) {
    message = f + ": wrong assignement: should be like ${var}=\"...\"\n";
    message += "line:" + line + "\n";
    return false;
  }

  if (start != 0) {
    message = f + ": wrong value declaration, declaration should start with: "
                  "${var}=\"...\"\n";
    message += "line:" + line + "\n";
    return false;
  }

  if (end == string::npos) {
    message =
        f +
        ": wrong value declaration, should be in brackets: ${var}=\"...\"\n";
    message += "line:" + line + "\n";
    return false;
  }

  string key = line.substr(0, end + 1);
  string val = line.substr((end) + var_end.length());
  trim(val);
  trim(key);

  if (!valid_varname(key)) {
    message = f + ": variable contains invalid characters.\n";
    message += "line:" + line + "\n";
    return false;
  }

  if (val.empty()) {
    message = f + ": empty declaration, not needed.\n";
    message += "line:" + line + "\n";
    return false;
  }

  if (!isquoted(val)) {
    message = f + ": value (" + val + ") in variable is not quoted.\n";
    message += "line:" + line + "\n";
    return false;
  }

  removequotes(val);
  evalvariables(val, variables);

  message = f + ": OK - assign to global map:" + key + "=" + val + "\n";
  variables[key] = val; // assign to global map

  return true;
}

// stray variables i.e not evaluated or initialize
// workaround until a proper design comes in
void strayvariable(string var, string line) {

  trim(var);
  size_t start = var.find(var_start);
  if (var.empty() || start == string::npos) {
    if (dbg)
      cerr << "dbg - strayvariable(" << var << "," << line
           << ") - empty variable." << endl;
    return;
  }

  char lc = var.at(var.length() - 1);
  int len = var.length() - 1;

  // vector variable
  if (datastore.find(var) != datastore.end()) {
    return;
  }

  //  ${}
  if (lc == var_end[0] && start == 0 && len == 2) {
    cerr << "error - invalid variable:" << var << endl;
    cerr << "line:" << line << endl;
    exit(11);
  }

  if (lc == var_end[0] && start == 0) {
    cerr << "error - stray variable:" << var << endl;
    cerr << "line:" << line << endl;
    exit(11);
  }
}

bool isvariable_name(string line) {

  string f = "isvariable_name()";
  trim(line);

  char lc = line.at(line.length() - 1);
  size_t start = line.find(var_start);

  action = "call " + f;
  message = action + " - line:" + line + "\n";

  if (start != 0) {
    message = f + ": wrong name, should start with: ${var}\n";
    message += "line:" + line + "\n";
    return false;
  }

  if (lc != '}') {
    message = f + ": wrong name, should be closed with brackets: ${var}\n";
    message += "line:" + line + "\n";
    return false;
  }

  if (!valid_varname(line)) {
    message = f + ": variable contains invalid characters.\n";
    message += "line:" + line + "\n";
    return false;
  }
  message = f + ": OK - valid variable name:" + line + "\n";

  return true;
}

// ${new}=${old}
bool isvariable_assign(string line) {
  string f = "isvariable_assign()";
  size_t start, end;
  char lc = line.at(line.length() - 1);

  if (line.find("=") == string::npos) {
    message = f + ": wrong assignement: should be like ${var1}=${var2}\n";
    message += "line:" + line + "\n";
    return false;
  }

  vector<string> pt = ssplit(line, '=');

  action = "call " + f;
  message = action + " - line:" + line + "\n";

  for (auto i = 0; i < pt.size(); i++) {
    trim(pt[i]);
    start = pt[i].find(var_start);
    end = pt[i].find("}");

    if (start != 0) {
      message = f + ": wrong declaration: " + pt[i] +
                " declaration should start with: ${var}\n";
      message += "line:" + line + "\n";
      return false;
    }

    if (end == string::npos) {
      message = f + ": wrong declaration, should be in brackets: ${var}\n";
      message += "line:" + line + "\n";
      return false;
    }
  }

  string key = pt[0];
  string val = pt[1];
  trim(val);
  trim(key);

  if (!valid_varname(key)) {
    message = f + ": variable contains invalid characters.\n";
    message += "line:" + line + "\n";
    return false;
  }

  if (variables.find(val) == variables.end()) {
    message =
        f + ": " + val + " is not defined. Predeclare before assignement.\n";
    message += "line:" + line + "\n";
    return false;
  }

  if (val.empty()) {
    message = f + ": empty declaration, not needed.\n";
    message += "line:" + line + "\n";
    return false;
  }

  removequotes(val);

  // we need to eval
  evalvariables(val, variables);

  message = f + ": OK - assign to global map:" + key + "=" + val + "\n";
  variables[key] = val; // assign to global map

  return true;
}

// get function name & args
bool functionparts(string line) {

  string f = "functionparts()";

  char lc = line.at(line.length() - 1);
  size_t start = line.find(func_start);
  function_name = line.substr(0, start);
  trim(function_name);
  if (dbg)
    cerr << f << " - process line:" << line << ", last character:" << lc
         << endl;

  if (start == string::npos) {
    message = f + ": no opening brackets in function call.\n";
    message += "line:" + line + "\n";
    return false;
  }

  if (lc != func_end[0]) {
    message = f + ": no closing brackets in function call.\n";
    message += "line:" + line + "\n";
    return false;
  }

  size_t end = line.rfind(func_end);
  string args = line.substr(start + 1, (end - start) - 1);
  // if ( args != " " )
  trim(args);

  // remove blanks around separators
  if (dbg)
    cerr << f << " - args:" << args << endl;
  delwhite(args, argdelim);
  if (dbg)
    cerr << f << " - args cleaned up:" << args << endl;
  function_args.clear();

  if (args.find(argdelim) != string::npos) {
    vector<string> v = csvsplit(args, argdelim);
    if (dbg)
      cerr << f << " - csvsplit returned " << v.size() << " elements" << endl;

    // we need to eval
    for (auto i = 0; i < v.size(); i++) {
      string s = trim(v[i]);
      evalvariables(s, variables);
      removedoublequotes(s);
      function_args.push_back(s);
    }
  } else {
    if (args.size() > 0) {
      evalvariables(args, variables);
      removedoublequotes(args);
      function_args.push_back(args);
    }
  }

  // check in functiontable
  if (functiontable.find(function_name) == functiontable.end()) {
    message = f + ": undefined function:" + function_name + ".\n";
    message += "line:" + line + "\n";
    return false;
  }

  return true;
}

// function without return value
bool isfunction(string line) {

  bool validfunction = false;
  ostringstream m;
  string f = "isfunction()";
  action = "call " + f;
  message = action + " - line:" + line + "\n";

  if (line.find(assignpattern) != string::npos) {
    message = f + ": this is a rvalue function (" + assignpattern + ")\n";
    message += "line:" + line + "\n";
    return false;
  }

  if (line.substr(0, 2) == var_start) {
    message = f + ": this is most likely a variable ... which hasn't passed "
                  "isvariable()\n";
    message += "line:" + line + "\n";
    return false;
  }

  if (!functionparts(line)) {
    message = f + ": error could not get function_name & args:" + line + "\n";
    return false;
  } else {
    message = f + ": OK\n" + line + "\n";
  }

  if (function_args.size() > 0) {
    m.str("");
    m << f << " - number of args:" << function_args.size() << endl;

    for (auto i = 0; i < function_args.size(); i++)
      m << i << ": " << function_args[i] + "\n";

    message += m.str();
  }

  return true;
}

bool isrvalue_function(string line) {

  if (line.find(assignpattern) == string::npos) {
    message = "isrvalue_function(): rvalue function needs assignment: (" +
              assignpattern + ")\n";
    message += "line:" + line + "\n";
    return false;
  }

  string f = "isrvalue_function()";
  action = "call " + f;
  message = action + " - line:" + line + "\n";

  vector<string> pt = ssplit(line, assignpattern);
  trim(pt[0]);
  trim(pt[1]);

  if (!isvariable_name(pt[0]))
    return false;

  returnvariable_name = pt[0];

  // get function name & args
  if (!functionparts(pt[1])) {
    message = f + ": error could not get function_name & args:" + line + "\n";
    return false;
  } else {
    message = f + ": OK\n" + line + "\n";
  }

  // cout <<"functionname:" << function_name << "|returnvariable_name:" <<
  // returnvariable_name <<endl;
  message = f + ": OK\n" + line + "\n";

  return true;
}

bool eval_arguments(vector<string> a, vector<string> &d) {

  int n = 0, replaced = 0;
  string func = "eval_arguments()";

  vector<string> store;
  ostringstream ss, m;
  string valid, invalid, value;

  action = "call " + func;

  for (auto i = 0; i < d.size(); i++) {
    int linenr = (i + 1);
    trim(d[i]);
    if (d[i][0] == '#' || d[i].empty())
      continue; // comments or empty lines

    size_t pos = d[i].find(var_start);
    size_t end = d[i].find('}');

    if (pos != string::npos && end != string::npos) {
      string part = d[i].substr(pos + var_start.length(),
                                (end - pos) - var_start.length());
      int num = s_toint(part, true);
      ss.str("");
      ss << var_start << num << "}";
      valid = ss.str();

      if (num != -1 && num >= a.size()) {
        ss.str("");
        ss << var_start << part << var_end;
        invalid = ss.str();

        m.str("");
        m << action << " - error on line " << linenr << ":\n >> " << d[i]
          << endl;

        if (d[i].find(invalid) != string::npos)
          m << " argument out of range and invalid assignment: " << invalid;
        else
          m << var_start << part << "}"
            << " is not defined on commandline. Usage: dp <script> [args]";

        message += m.str();
        return false;
      }

      if (num != -1) {
        size_t p_equal = d[i].find('=');

        if (p_equal != string::npos && d[i].find(valid) != string::npos) {
          m.str("");
          m << action << " - error on line " << linenr << ":\n >> " << d[i]
            << endl;
          m << "arguments cannot be overwritten\n";
          message += m.str();
          return false;
        }
      }
    }

    for (auto n = 0; n < a.size(); n++) {
      value = a[n];
      ss.str("");
      ss << var_start << n << "}";
      valid = ss.str();
      size_t p1 = d[i].find(valid);
      size_t p2 = valid.find('=');

      if (p1 != string::npos && p2 != string::npos) {
        cerr << valid << "p1:" << p1 << "|p2" << p2 << endl;
        m.str("");
        m << action << " - error on line " << linenr << ":\n >> " << d[i]
          << endl;
        m << "arguments cannot be overwritten\n";
        message += m.str();
        return false;
      }

      if (p1 != string::npos) {
        if (string_replace(d[i], valid, value))
          replaced++;
      }
    }

    store.push_back(d[i]);

    if (dbg)
      cerr << "dbg - " << func << ": transformed line:" << d[i] << endl
           << replaced << " variables replaced\n";
  }
  d = store;
  return true;
}

void numericalsort_asc(vector<string> &v, string args) {
  // maps sort automatically by key, greater<double> as a third argument results
  // in ascending order
  multimap<double, string, less<double>> m;
  multimap<double, string>::iterator it;
  ostringstream ss, tmp;
  int col = s_toint(args);
  col--;

  if (col < 0) {
    cerr << "error - numericalsort(): argument (" << args
         << ") must be larger than zero or is invalid.\n";
    return;
  }
  ss.setf(ios::fixed);

  for (auto i = 0; i < v.size(); i++) {
    vector<string> o = csvsplit(v[i], CSVDELIM);
    string val;
    double size;
    if (o.size() <= col)
      continue;
    if (o[col].empty())
      continue;
    size = atof(o[col].c_str());
    val = v[i];
    sremove(val, o[col]);
    rtrim(val, CSVDELIM);
    pair<double, string> p(size, val);
    m.insert(p);
  }
  v.clear();

  for (it = m.begin(); it != m.end(); it++) {
    tmp.clear();
    tmp.str("");
    vector<string> v1;
    tmp << setprecision(15) << it->first;
    string number = tmp.str();
    v1 = csvsplit(it->second, CSVDELIM);
    v1.insert(v1.begin() + col, number);

    for (auto i = 0; i < v1.size(); i++) {
      trim(v1[i]);
      if (v1[i].empty())
        continue;
      if (i == v1.size() - 1)
        ss << v1[i];
      else
        ss << v1[i] << CSVDELIM;
    }
    v.push_back(ss.str());
    ss.clear();
    ss.str("");
  }
}

void numericalsort_desc(vector<string> &v, string args) {

  // maps sort automatically by key, greater<double> as a third argument results
  // in ascending order
  multimap<double, string, greater<double>> m;
  multimap<double, string>::iterator it;
  ostringstream ss, tmp;
  int col = s_toint(args);
  col--;

  if (col < 0) {
    cerr << "error - numericalsort(): argument (" << args
         << ") must be larger than zero or is invalid.\n";
    return;
  }
  ss.setf(ios::fixed);

  for (auto i = 0; i < v.size(); i++) {
    vector<string> o = csvsplit(v[i], CSVDELIM);
    string val;
    double size;
    if (o.size() <= col)
      continue;
    if (o[col].empty())
      continue;
    size = atof(o[col].c_str());
    val = v[i];
    sremove(val, o[col]);
    rtrim(val, CSVDELIM);
    pair<double, string> p(size, val);
    m.insert(p);
  }
  v.clear();

  for (it = m.begin(); it != m.end(); it++) {
    tmp.clear();
    tmp.str("");
    vector<string> v1;
    tmp << setprecision(15) << it->first;
    string number = tmp.str();
    v1 = csvsplit(it->second, CSVDELIM);
    v1.insert(v1.begin() + col, number);

    for (auto i = 0; i < v1.size(); i++) {
      trim(v1[i]);
      if (v1[i].empty())
        continue;
      if (i == v1.size() - 1)
        ss << v1[i];
      else
        ss << v1[i] << CSVDELIM;
    }
    v.push_back(ss.str());
    ss.clear();
    ss.str("");
  }
}

void register_functions() {}

//--------------------------------------------------------------
// the main function
//--------------------------------------------------------------
void parser(string script, map<string, int> ft, vector<string> a) {

  vector<string> data, parts, lines;
  args = a;
  bool validargs = false;

  functiontable = ft;
  map<string, int>::iterator it;
  map<string, vector<string>>::iterator itf;

  string assign_variable_name;
  SCRIPT = script;

  register_functions();

  int linenr = 0;
  action = "starting";

  // we have a file
  if (file_exists(script)) {
    lines = readfile(script, false);
  } else {
    lines = process_args(script, validargs);
    if (!validargs)
      syntax();
  }

  // evaluate and set commandline arguments
  if (!eval_arguments(a, lines)) {
    cerr << message << endl;
    return;
  }

  // get through the script and do something
  for (auto i = 0; i < lines.size(); i++) {
    string line = trim(lines[i]);
    original_lines[line] = linenr++;

    if (dbg) {
      cerr << "-----------------------------" << endl;
      cerr << "process line: " << line << endl;
      cerr << "last action: " << action << endl;
    }

    if (line.empty() || iscomment(line)) {
      if (dbg)
        cerr << "skip:" << line << endl;
      linenr++;
      continue;
    }

    if (dbg)
      cerr << "STEP1: try isvariable():" << line << endl;
    if (isvariable(line)) {
      if (variables["${debug}"] == "true") {
        dbg = true;
      }
      if (dbg)
        cerr << message << endl;
      linenr++;
      continue;
    }

    if (dbg)
      cerr << "STEP2: try isvariable_assign():" << line << endl;
    if (isvariable_assign(line)) {
      if (dbg)
        cerr << message << endl;
      linenr++;
      continue;
    }

    if (dbg)
      cerr << "STEP3: try isfunction():" << line << endl;
    if (isfunction(line)) {

      if (dbg)
        cerr << message << endl;

      if (dbg) {
        cerr << "function_args.size():" << function_args.size() << endl;
      }

      for (auto x = 0; x < function_args.size(); x++) {
        strayvariable(function_args[x], line);
      }

      if (functiontable[function_name] != function_args.size()) {
        cerr << "error: " << function_name
             << "() wrong number of arguments: " << function_args.size()
             << " - must be " << functiontable[function_name] << endl
             << "line:" << line << endl;
        exit(10);
      }

      if (function_name == "timer_start")
        timer_start = mticks();
      if (function_name == "timer_end") {
        timer_end = mticks();
        cerr.precision(4);
        cerr << "elapsed:" << (timer_end - timer_start) / 1000 << " secs \n";
      }

      if (function_name == "readfile")
        data = readfile(function_args[0]);
      if (function_name == "read_match")
        data = read_match(function_args[0], function_args[1]);
      if (function_name == "writefile")
        writefile(data, function_args[0]);
      if (function_name == "appendfile")
        appendfile(data, function_args[0]);
      if (function_name == "writedata")
        writedata(function_args[0], function_args[1]);
      if (function_name == "appenddata")
        appenddata(function_args[0], function_args[1]);
      if (function_name == "showvariables")
        mp(variables);
      if (function_name == "tail")
        tail(data, function_args[0]);
      if (function_name == "head")
        head(data, function_args[0]);
      if (function_name == "command2data")
        data = lib::runcmd(function_args[0], true);
      if (function_name == "command")
        if (!command(function_args, message))
          exit(30);
      if (function_name == "stdout")
        vp(data);
      if (function_name == "print")
        print(function_args);
      if (function_name == "count")
        cout << data.size() << endl;
      if (function_name == "count_data")
        cout << function_args[0] << ":" << datastore[function_args[0]].size()
             << endl;
      if (function_name == "remove_empty")
        remove_empty(data);
      if (function_name == "order")
        order(data, function_args[0]);
      if (function_name == "unique")
        vec_uniquesort(data);
      if (function_name == "remove")
        match(data, function_args[0], "remove", "false");
      if (function_name == "match")
        match(data, function_args[0], "match", function_args[1]);
      if (function_name == "match_column")
        match_column(data, function_args[0], function_args[1],
                     function_args[2]);
      if (function_name == "merge")
        merge(data, function_args[0], function_args[1][0]);
      if (function_name == "simplejoin")
        simplejoin(data, function_args);
      if (function_name == "setcsvseparator")
        setcsvseparator(function_args[0]);
      if (function_name == "setcolumnnames")
        setcolumnnames(function_args);
      if (function_name == "sum")
        find_number(data, function_args[0], function_args[1], "sum");
      if (function_name == "sum_fs")
        sum_fs(data, function_args[0], function_args[1], function_args[2],
               function_args[3]);
      if (function_name == "fs_usage")
        fs_usage(data, function_args[0], function_args[1], function_args[2]);
      if (function_name == "find_highest")
        find_number(data, function_args[0], function_args[1], "highest");
      if (function_name == "find_lowest")
        find_number(data, function_args[0], function_args[1], "lowest");
      if (function_name == "columns")
        columns(data, function_args[0], CSVDELIM);
      if (function_name == "exit")
        exit(0);

      if (function_name == "numericalsort") {

        if (function_args[1] == "asc")
          numericalsort_asc(data, function_args[0]);
        if (function_args[1] == "desc")
          numericalsort_desc(data, function_args[0]);
        if (function_args[1] != "asc" && function_args[1] != "desc") {
          cerr << "error - numericalsort(): second argument must be asc or desc"
               << endl;
          exit(10);
        }
      }
      if (function_name == "math") {
        if (function_args[0].empty()) {
          cerr << "error -  math(): first argument is empty." << endl;
          exit(10);
        }

        if (function_args[1].empty()) {
          cerr << "error -  math(): second argument is empty." << endl;
          exit(10);
        }
        math(data, function_args[0], function_args[1]);
      }
      if (function_name == "match_data") {
        if (datastore[function_args[0]].size() == 0) {
          cerr << "error -  match_data(): data empty." << endl;
          exit(10);
        }

        if (function_args[1] != "match" && function_args[1] != "remove") {
          cerr << "error -  match_data(): second argument must either be match "
                  "or remove."
               << endl;
          exit(10);
        }

        match_data(data, datastore[function_args[0]], function_args[1]);
      }

      // data management
      if (function_name == "setdata")
        setdata(data, function_args[0]);
      if (function_name == "insert") {
        data.insert(data.begin(), function_args[0]);
      }
      if (function_name == "append") {
        data.push_back(function_args[0]);
      }
      continue;
    }

    if (dbg)
      cerr << "STEP4: try isrvalue_function():" << line << endl;
    if (isrvalue_function(line)) {

      for (auto x = 0; x < function_args.size(); x++)
        strayvariable(function_args[x], line);

      if (dbg)
        cerr << message << endl;
      // do something
      if (function_name == "readfile")
        datastore[returnvariable_name] = readfile(function_args[0]);
      if (function_name == "grep")
        datastore[returnvariable_name] =
            grep(data, function_args[0], function_args[1]);
      if (function_name == "grep_column")
        datastore[returnvariable_name] = grep_column(
            data, function_args[0], function_args[1], function_args[2]);
      if (function_name == "epoch")
        variables[returnvariable_name] = epoch(function_args);
      if (function_name == "test")
        variables[returnvariable_name] = function_args[0];
      if (function_name == "command2line") {
        if (dbg)
          cerr << "returnvariable_name:" << returnvariable_name << endl;
        variables[returnvariable_name] = command2line(function_args);
        if (dbg)
          cerr << "command2line() returned (variables[returnvariable_name]):"
               << variables[returnvariable_name] << endl;
      }

      if (function_name == "command2data")
        datastore[returnvariable_name] = lib::runcmd(function_args[0], true);
      continue;
    }

    // someting went wrong
    cerr << "[FATAL] syntax error: could not interpret line:" << line << "\n";
    syntax();
  }
}

} // namespace parser
