#include <iostream>
#include <fstream>
#include <iterator>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <cmath>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>
#include <sstream>
#include <sys/time.h>


// compile with  -std=c++0x

using namespace std;


namespace lib {

// string processing functions
void srev ( string &s); //reverses a string
string &ltrim(string &s);
string &ltrim(string &s, char c);
string &rtrim(string &s);
string &rtrim(string &s, char c);
string &trim(string &s);
void delwhite( string& str, string p); // trims all whitespace around characters provided by p
void delwhite( string& str, char c); // trims all whitespace around character

string replace(string str, const string& from, const string& to);
void sreplace(string &s, char search, char replace);
bool sreplace(string& str, const string& from, const string& to); // bug: replaces only first occurance! use replace(...)


void sremove( string &s, char c );
void sremove(string &s, string p);

string chomp(string str);
string cutout(string s, char start, char end);
void cutreplace(string &s, char start, char end, char replace);
vector<string> split(string s, string delim);
vector<string> ssplit(const string &text, char sep);
vector<string> ssplit(const string &text, string sep);
vector<string> ssplit(const string &s, char sep, bool trimwhite);
long s_tolong(string s);
long s_tolong(string s, bool strict);
double s_todouble(string s);
int s_toint(string s);
int s_toint(string s, bool strict);
void remove_dupl(string &s, char c);
void remove_dupl(string &s);
string cutstr(string s, string start, char end);
bool isquoted(string s);

// find / pattern matching
bool ci_find(string s, string p);
bool ci_find(string s, string p, bool loose);
bool ci_find(string s, vector<string> v, string type);

// string search functions
bool search_all(string s, vector<string>pattern, bool casesensitive);
bool search_any(string s, vector<string>pattern, bool casesensitive);
bool match(string s, string pattern);
string find_first_number(string s);


// file & data processing
vector<string> flatten (vector<string>v, vector<string> skip, string record_delim, bool addspace);
vector <string> word(string s);
vector <string> word(char * file);
string f_dirname(string source);
string f_basename(string source);
bool file_exists (char * file);
bool file_exists (string file);
string f_ext(char * f); // returns file extension
string f_ext(string f); // returns file extension
long f_mtime(string f); // returns mtime
long f_age(string f); // file age based on mtime
long f_age(string f, char type); // file age
long f_stat(string f, char flag); // stat wrapper
int recursedir(const char *path); // ftw based recursive traversal
bool isfullpath(string s);

// date
string month_convert(string s);
string convert_date(string s);

// csv 
int advquoted(const string& s, string& fld, int i, char sep);
int advplain(const string& s, string& fld, int i, char sep);
vector<string> csvsplit(const string &s, char sep);
vector<string> csvsplit(const string &s, char sep, char replace);
vector<string> csvsplit(const string &s, char sep, bool delete_comma);


// processes
vector<string> runcmd(string cmd, bool stripwhite);

// timer
double mticks();
double t_start();
double t_end(double start);
void _t(double start, double end);

// helpers
void vp(vector<string>v);

} // end namespace lib