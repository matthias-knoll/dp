#include <iostream>
#include <fstream>
#include <iterator>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <cmath>
#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>
#include <sstream>
#include <sys/time.h>
#include <ftw.h>
#include <unistd.h>

using namespace std;

namespace lib {

//--------------------------------------
// timer functions
/*
example - simple timing:
double start=t_start();
somefunc();
cout << t_end(start) << " ms" << endl;

*/

double mticks() {
    struct timeval tv;
    gettimeofday(&tv, 0);
    return (double) tv.tv_usec / 1000 + tv.tv_sec * 1000;
}

double t_start() {
	return mticks();
}

double t_end(double start) {
	double end=mticks();	
	return (end-start);
}

void _t(double start, double end) {
	cout.precision(2);
	cout << (end-start) << " ms \n";
}


//--------------------------------------
// time and date
string month_convert(string s) {
	map<string,string>m;
	m["JAN"] = ".01.";
	m["FEB"] = ".02.";
	m["MAR"] = ".03.";
	m["APR"] = ".04.";
	m["MAY"] = ".05.";
	m["JUN"] = ".06.";
	m["JUL"] = ".07.";
	m["AUG"] = ".08.";
	m["SEP"] = ".09.";
	m["OCT"] = ".10.";
	m["NOV"] = ".11.";
	m["DEC"] = ".12.";

return m[s];
}

// convert 28JUN12 to 28.06.2012
string convert_date(string s) {
	string day = s.substr(0,2);
	string month = month_convert (s.substr(2,3) );
	string year = s.substr(5,2);
return day + month + year;
}


//--------------------------------------
// string processing

struct both_white {
    bool operator()(char a, char b) const {
        // return a == ' ' && b == ' ';
		return isspace(a) && isspace(b);
    }
};


void remove_dupl(string &s) {
	s.erase(std::unique(s.begin(), s.end()), s.end());
}

void remove_dupl(string &s, char e) {
	string result="";
	char b,c;
	for (auto i=0; i<s.length(); i++) {
		c=s[i];
		if ( i>0 ) b=s[i-1]; // before character
		if ( c != e ) {
			result+=c;
			continue;
		}
		
		if ( c==e && b!=e ) { result+=c; }
    }
	s=result;
}

// extracts the first sequence of numbers from a string, skips punctuation, i.e is 
// not detecting float
string find_first_number(string s) {
	string r="";
	char c;
	for (auto i=0; i<s.length();i++) {
		c=s[i];
		if ( isdigit(c) ) r+=c;
		if ( isalpha(c) && r.length() > 0 ) break;
	}
return r;
}

// extracts the first sequence of numbers from a string, skips punctuation, i.e is 
// not detecting float
long s_tolong(string s) {
	string r="";
	long n=-1;
	char c;
	for (auto i=0; i<s.length();i++) {
		c=s[i];
		if ( isdigit(c) ) r+=c;
		if ( isalpha(c) && r.length() > 0 ) break;
	}
	
	if ( r != "" )
		n=stol(r);
	
return n;
}

// does not allow punctuation or anything else than digits
long s_tolong(string s, bool strict) {
	string r="";
	long n=-1;
	char c;
	for (auto i=0; i<s.length();i++) {
		c=s[i];
		if ( ! isdigit(c) ) return -1;
		r+=c;
	}
	
	if ( r != "" )
		n=stol(r);
	
return n;
}

// does not allow punctuation or anything else than digits
int s_toint(string s, bool strict) {	
	string r="";
	int n=-1;
	char c;
	for (auto i=0; i<s.length();i++) {
		c=s[i];
		if ( ! isdigit(c) ) { return -1; }			
		r+=c;
	}
	
	if ( r != "" )
		n=stoi(r);
	
return n;
}

int s_toint(string s) {	
	string r="";
	int n=-1;
	char c;
	for (auto i=0; i<s.length();i++) {
		c=s[i];
		
		if ( ispunct(c) ) { return -1; }
		
		if ( isdigit(c) ) r+=c;
		if ( isalpha(c) && r.length() > 0 ) break;
	}
	
	if ( r != "" )
		n=stoi(r);
	
return n;
}

// extracts the first sequence of numbers from a string
double s_todouble(string s) {
	string r="";
	double n=-1;
	char c;
	int punctcnt=0;
	
	for (auto i=0; i<s.length();i++) {
		c=s[i];
		if (ispunct(c)) {
			r+=c;
			punctcnt++;
			if ( punctcnt > 1 ) break;
		}
		if ( isdigit(c) ) r+=c;
		if ( isalpha(c) && r.length() > 0 ) break;
	}
	
	if ( r != "" ) {
		//cerr << "dbg s_todouble:" << r << endl;
		n=stod(r);
	}
return n;
}

/*
// strips both leading and trailing whitespace, slower than trim()
string wtrim(const std::string& str, const std::string& whitespace = " \t") {
    const auto strBegin = str.find_first_not_of(whitespace);
    if (strBegin == std::string::npos)
        return ""; // no content

    const auto strEnd = str.find_last_not_of(whitespace);
    const auto strRange = strEnd - strBegin + 1;

    return str.substr(strBegin, strRange);
}
*/

// strips all whitespace around a set of characters, keeping whitespace in words (depending on the characters in p)
void delwhite(string& s, string sep) {    
	
	string out;
	int start = 0, end = 0;
	
	for (auto i=0; i<sep.length(); i++) {
		char c=sep[i];
		if (isspace(c)) continue;
		
		// string split
		vector<string> v;
		// reset 
		start=0;
		end=0;
		v.clear();
		
		while ((end = s.find(c, start)) != string::npos) {
			v.push_back(s.substr(start, end - start));
			start = end + 1;
		}
		v.push_back(s.substr(start));
		// end string split
		
		for (auto i=0; i<v.size(); i++ ) {
			// trim trailing & leading whitespace
			v[i].erase(v[i].begin(), std::find_if(v[i].begin(), v[i].end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
			v[i].erase(std::find_if(v[i].rbegin(), v[i].rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), v[i].end());
			
			if (i==0) 
				out=v[i];
			else
				out+=v[i];
			
			if ( i<(v.size()-1) ) out+=c;
		}		
		s=out;
	}
	
return;
}

// strips all whitespace around a characters keeping whitespace in words
void delwhite(string& s, char c) {    
	
	string out;
	int start = 0, end = 0;
		
	if (isspace(c)) return; // do nothing
	
	// string split
	vector<string> v;
	// reset 
	start=0;
	end=0;
	v.clear();
	
	while ((end = s.find(c, start)) != string::npos) {
		v.push_back(s.substr(start, end - start));
		start = end + 1;
	}
	v.push_back(s.substr(start));
	// end string split
	
	for (auto i=0; i<v.size(); i++ ) {
		// trim trailing & leading whitespace
		v[i].erase(v[i].begin(), std::find_if(v[i].begin(), v[i].end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
		v[i].erase(std::find_if(v[i].rbegin(), v[i].rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), v[i].end());
		
		if (i==0) 
			out=v[i];
		else
			out+=v[i];
		
		if ( i<(v.size()-1) ) out+=c;
	}		
	s=out;
	
return;
}

void srev(string & s) {
	string::reverse_iterator rit;
	string r = "";
	for ( rit=s.rbegin(); rit < s.rend(); rit++ )
		r += *rit;

	s=r;
}

// trim from start
string &ltrim(string &s) {
	s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
return s;
}

// trim from end
string &rtrim(string &s) {
	s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
return s;
}

// trim from end
string &rtrim(string &s, char c) {
	if ( s.size() > 0 && s.at( s.length()-1 )==c )  s.resize (s.size()-1);
return s;
}

// trim from end
string &ltrim(string &s, char c) {
	if ( s.size() > 0 && s.at( 0 )==c )  s.erase(0);
return s;
}

// trim trailing and leading whitespace
string &trim(string &s) {
	return ltrim(rtrim(s));
}

vector<string> ssplit(const string &s, char sep) {
  int start = 0, end = 0;
  vector<string> t;

  while ((end = s.find(sep, start)) != string::npos) {
    t.push_back(s.substr(start, end - start));
    start = end + 1;
  }
  t.push_back(s.substr(start));
 return t;
}

vector<string> ssplit(const string &s, char sep, bool trimwhite) {
  int start = 0, end = 0;
  string part="";
  vector<string> t;

  while ((end = s.find(sep, start)) != string::npos) {
	if ( trimwhite ) {
		part=s.substr(start, end - start);
		trim(part);
		t.push_back(part);
	} else {	
		t.push_back(s.substr(start, end - start));
	}
    start = end + 1;
  }
  
  if ( trimwhite ) {
		part=s.substr(start);
		trim(part);
		t.push_back(part);
  } else
	t.push_back(s.substr(start));

 return t;
}


vector<string> ssplit(const string &s, string sep) {
  int start = 0, end = 0;

  vector<string> t;

  while ((end = s.find(sep, start)) != string::npos) {
    t.push_back( s.substr( start, end - start ) );
    start = end + sep.length();
  }
  t.push_back( s.substr( start ) );
 return t;
}


vector<string> split( string s, char delim) {
    vector <string> v;
	stringstream ss(s);
    string item;

	while(getline(ss, item, delim)) {
        v.push_back(item);
    }
return v;
}

/*
 example - this will match:
 if ( match("Achtung Baby, U2 record", "u2|rec|acht")) cout << "match!" << endl;

*/
bool match(string s, string p) {
	vector <string> v=ssplit(p, "|");

	bool state=false;

	for (int i=0; i<v.size(); i++) {
		transform(s.begin(), s.end(), s.begin(), ::tolower);
		transform(v[i].begin(),v[i].end(), v[i].begin(), ::tolower);

		if ( s.find(v[i]) != string::npos ) state=true;
	}

return state;
}

// returns a vector of words from a file
vector <string> word(char * file) {

	ifstream ifs(file);
	istream_iterator< string > is( ifs );
	istream_iterator< string > eof;
	map <string,int> m;
    set <string> exclude = { "a", "the", "The", "But", "but", "or", "and", "And", "By", "by", "is", "Is", "Are", "are", "To", "to" };

	vector <string> text;
	copy( is, eof, back_inserter( text ));

return text;
}

// returns a vector of words from a string
vector <string> word(string s) {
	std::vector<std::string> w;
	istringstream iss(s);

	copy(istream_iterator<string>(iss), istream_iterator<string>(), back_inserter(w));

return w;
}

static int callback(const char *fpath, const struct stat *sb, int typeflag) {
	printf("%s\n", fpath);
	return 0;
}


int recursedir(const char *path) {
	ftw(path, callback, 16);
return 0;
}


string chomp(string str) {
	str.erase(std::remove(str.begin(), str.end(), '\r'), str.end());
	str.erase(std::remove(str.begin(), str.end(), '\n'), str.end());
return trim(str);
}

string cutout(string s, char start, char end) {
	
	size_t first = s.find(start);
	size_t last = s.find(end, first);

	if ( first == string::npos || last == string::npos ) return s;

	s.erase(first,last-first+1);
return s;
}

void cutreplace(string &s, char start, char end, char replace) {
	size_t first = s.find(start);
	size_t last = s.find(end, first);
	string r=s;

	if ( first == string::npos || last == string::npos ) return ;

	while ( first !=string::npos && last !=string::npos ) {
		s.insert(last+1,1,replace);
		s.erase(first,last-first+1);
		first = s.find(start);
		last = s.find(end, first);
	}
}

void sreplace( string &s, char c, char r) {
  std::replace( s.begin(), s.end(), c, r); // replace all c to r
}

string replace(string str, const string& from, const string& to) {
    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
    }
return str;
}

// bug: replaces only first occurance - use replace()!!
bool sreplace(string& str, const string& from, const string& to) {
    size_t start_pos = str.find(from);
    if(start_pos == string::npos)
        return false;
    str.replace(start_pos, from.length(), to);
    return true;
}

void sremove( string &str, char c ) {
	str.erase(std::remove(str.begin(), str.end(), c), str.end());
}

void sremove(string &s, string p) {
	
	std::string::size_type i = s.find(p);
	while (i != std::string::npos) {
		s.erase(i, p.length());
		i = s.find(p, i);
   }
return;
}


// advquoted: quoted field; return index of next separator
int advquoted(const string& s, string& fld, int i, char sep) {
	int j;

	fld = "";
	for (j = i; j < s.length(); j++) {
		if (s[j] == '"' && s[++j] != '"') {
			int k = s.find_first_of(sep, j);
			if (k > s.length())	// no separator found
				k = s.length();
			for (k -= j; k-- > 0; )
				fld += s[j++];
			break;
		}
		fld += s[j];
	}
	return j;
}

// advplain: unquoted field; return index of next separator
int advplain(const string& s, string& fld, int i, char sep) {
	int j;

	j = s.find_first_of(sep, i); // look for separator
	if (j > s.length())               // none found
		j = s.length();
	fld = string(s, i, j-i);
	return j;
}


vector<string> csvsplit(const string &s, char sep) {

	string fld;
	int i, j, nfield = 0;
	vector<string>v;

	if (s.length() == 0)
		return v;
	i = 0;

	do {
		if (i < s.length() && s[i] == '"')
			j = advquoted(s, fld, ++i, sep);
		else
			j = advplain(s, fld, i, sep);
		if (nfield >= v.size())
			v.push_back(fld);
		else
			v[nfield] = fld;
		nfield++;
		i = j + 1;
	} while (j < s.length());

return v;
}

vector<string> csvsplit(const string &s, char sep, char replace) {

	string fld;
	int i, j, nfield = 0;
	vector<string>v;

	if (s.length() == 0)
		return v;
	i = 0;

	do {
		if (i < s.length() && s[i] == '"')
			j = advquoted(s, fld, ++i, sep);
		else
			j = advplain(s, fld, i, sep);
		if (nfield >= v.size()) {
			sreplace(fld, sep, replace);
			v.push_back(fld);
		}
		else
			v[nfield] = fld;
		nfield++;
		i = j + 1;
	} while (j < s.length());

return v;
}

vector<string> csvsplit(const string &s, char sep, bool delete_comma) {

	string fld;
	int i, j, nfield = 0;
	vector<string>v;

	if (s.length() == 0)
		return v;
	i = 0;

	do {
		if (i < s.length() && s[i] == '"')
			j = advquoted(s, fld, ++i, sep);
		else
			j = advplain(s, fld, i, sep);
		if (nfield >= v.size()) {
			sremove(fld, sep);
			v.push_back(fld);
		}
		else
			v[nfield] = fld;
		nfield++;
		i = j + 1;
	} while (j < s.length());

return v;
}

bool ci_find(string s, string p) {
	
	bool state=false;
	
	transform(s.begin(), s.end(), s.begin(), ::tolower);
	transform(p.begin(),p.end(), p.begin(), ::tolower);

	if ( s.find(p) != string::npos ) state=true;
	
	//cerr << "dbg ci_find() string " << state << "\n";	
	
return state;
}

// match with several search pattern and logical operation defined by type(AND / OR)
bool ci_find(string s, vector<string> search_words, string logical_operator) {

	bool state=false;
	int cnt=0;
	string p;
	
	for ( auto i=0; i<search_words.size(); i++) {
		p=trim(search_words[i]);
		transform(s.begin(), s.end(), s.begin(), ::tolower);
		transform(p.begin(),p.end(), p.begin(), ::tolower);		
		if ( s.find(p) != string::npos ) cnt++;		
	}
	
	if ( logical_operator=="AND" && cnt==search_words.size() ) state=true;
	if ( logical_operator=="OR" && cnt>0 ) state=true;
	if ( logical_operator=="" && cnt>0 ) state=true;
	
	//if ( state )
	//cerr << "dbg ci_find(): op.:"<< logical_operator << " substr.:" << s.substr(0,14) << "... found:" << cnt << " state:" << state <<endl;

return state;
}

bool ci_find(string s, string p, bool loose) {

	bool state=false;	
	transform(s.begin(), s.end(), s.begin(), ::tolower);
	transform(p.begin(),p.end(), p.begin(), ::tolower);

	if ( s.find(p) != string::npos ) state=true;
	
	//cerr << "dbg ci_find() string matchoption " << state << "\n";	
	
return state;
}

string cutstr(string s, string start, char end) {
	size_t first = s.find(start);
	size_t last = s.find(end, first);
	string result="";

	if ( first == string::npos || last == string::npos ) return "";

	int l=(last-first)-start.length();

	result=s.substr( first+start.length(), l);

return trim(result);
}

bool isquoted(string s) {
	trim(s);
	if( s.empty() ) return false;
	
	if ( s[0] == '\"' && s[s.length()-1] == '\"' ) return true;

return false;
}

bool isfullpath(string s) {
	trim(s);
	if( s.empty() ) return false;
	sremove(s, '"');
	if ( s[0] == '/' ) return true;

return false;
}

//---------------------------------------------
// files and directories

bool file_exists (char * file) {

	ifstream ifs(file);
	if ( ifs.fail() ) return false;

return true;
}

bool file_exists (string file) {

	ifstream ifs(file.c_str() );
	if ( ifs.fail() ) return false;

return true;
}


long f_mtime(string f) {

	long mtime=0;
	
	if ( ! file_exists(f) ) return 0;
	
	struct stat result;
	if (stat(f.c_str(), &result)==0) {
		mtime = result.st_mtime;
	}

return mtime;
}

long f_stat(string f, char flag) {

	long atime=0;
	long ctime=0;
	long mtime=0;
	long t=0;
	
	if ( ! file_exists(f) ) return 0;
	
	struct stat result;
	if (stat(f.c_str(), &result)==0) {
		
		switch(flag) {
			case 'a': t = result.st_atime;
			case 'c': t = result.st_ctime;
			case 'm': t = result.st_mtime;
		}
	}

return t;
}

long f_age(string f) {

	time_t mtime=0;
	time_t now;
	time_t age=0;
	
	if ( ! file_exists(f) ) return 0;
	mtime=f_mtime(f.c_str());   
    time(&now);

return (now-mtime);
}

long f_age(string f, char type) {

	time_t t=0;
	time_t now;
	time_t age=0;
	
	if ( ! file_exists(f) ) return 0;	
	t = f_stat(f,type);
    time(&now);

return (now-t);
}

string f_ext(char * file) {
	string f=string(file);
	size_t dot = f.find_last_of(".");
	return f.substr(dot);
}

string f_ext(string f) {
	size_t dot = f.find_last_of(".");
	return f.substr(dot);
}


struct pathseparator {
    bool operator()( char ch ) const    {
        return ch == '\\' || ch == '/';
    }
};

string f_dirname(string source) {
    source.erase(std::find(source.rbegin(), source.rend(), '/').base(), source.end());
    return source;
}

string f_basename(string source) {    
    return std::string( std::find_if( source.rbegin(), source.rend(), pathseparator() ).base(), source.end() );
}

//---------------------------------------------
// processes

// run commands
vector<string> runcmd(string cmd, bool stripwhite) {
	vector<string>v;
	//cerr << "runcmd(): function disabled!"<<endl;
	v.push_back("");
	
	int n=0;
	FILE* pipe = popen(cmd.c_str(), "r");
    char buffer[128];
	string s;
	if (!pipe) return v;

    while(!feof(pipe)) {
    	if(fgets(buffer, 128, pipe) != NULL) {
			s=string(buffer);
			s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
			if ( stripwhite ) {
				s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
			}
			v.push_back(s);
		}
    }
    pclose(pipe);
    return v;
}


bool BothAreSpaces(char lhs, char rhs) { return (lhs == rhs) && (lhs == ' '); }

// flatten data structure from newline separated files to single lines per record
vector<string> flatten(vector<string>v, vector<string> skip, string record_delim, bool addspace) {

	string str, buffer;
	vector<string>flat;
	int found;

	for ( auto i=0; i<v.size(); i++ ) {
		found=0;
		str=v[i];

		for ( auto it=skip.begin(); it!=skip.end(); ++it)
			if ( str.find(*it) != string::npos ) found++;

		if ( found ) {
			continue;
		}

		str.erase(std::remove(str.begin(), str.end(), '\r'), str.end());
		str.erase(std::remove(str.begin(), str.end(), '\n'), str.end());
		str.erase(std::remove(str.begin(), str.end(), '\\'), str.end());

		// empty newline
		if ( str == record_delim ) {
			if ( trim(buffer) != "" ) {
				string::iterator new_end = std::unique(buffer.begin(), buffer.end(), BothAreSpaces);
				buffer.erase(new_end, buffer.end());
				flat.push_back(buffer);
				buffer="";
			}
			continue;
		}
		if ( addspace ) buffer+=" ";  // from flattened multiline
		buffer+=trim(str);
	}
	// last line
	if ( trim(buffer) != "" ) flat.push_back(buffer);
return flat;
}

void vp(vector<string>v) {
	int n=1;
	for (auto i = v.begin(); i != v.end(); ++i) 
		cout << *i << endl;
}

bool search_any(string s, vector<string>pattern, bool casesensitive) {
	
	if ( ! casesensitive ) {
		 transform(s.begin(), s.end(), s.begin(), ::tolower);
		 for( int i=0; i<pattern.size(); i++) {
			transform(pattern[i].begin(), pattern[i].end(), pattern[i].begin(), ::tolower);
			cerr << i << ":" << pattern[i] << endl;
		}	
	}
	
    string found_target; // set to the target we found, if we found any
    for( vector<string>::const_iterator it = pattern.begin(); found_target.empty() && (it != pattern.end()); ++it )  {
        if( s.find(*it) != string::npos )
            found_target = *it;
    }
    if( ! found_target.empty() )
      return true;
  
return false;    
}

bool search_all(string s, vector<string>pattern, bool casesensitive) {
	
	if ( ! casesensitive ) {
		 transform(s.begin(), s.end(), s.begin(), ::tolower);
		 for( int i=0; i<pattern.size(); i++) {
			transform(pattern[i].begin(), pattern[i].end(), pattern[i].begin(), ::tolower);
			cerr << i << ":" << pattern[i] << endl;
		}	
	}
	
	int found=0;
	
    string found_target; // set to the target we found, if we found any
    for( vector<string>::const_iterator it = pattern.begin(); found_target.empty() && (it != pattern.end()); ++it )  {
        if( s.find(*it) != string::npos )
            found++;
    }
    if( found == pattern.size() )
      return true;
  
return false;    
}

} // end namespace lib