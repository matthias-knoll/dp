
DESCRIPTION
===========
dp2 is a processor for delimited ascii text, typically csv processing. It supports
variables and functions but no control structures. Any separated data can be processed, thus the default separator
has to be overwritten by setcsvseparator().

USAGE
=========
example:

dp2 '{ readfile("myfile.csv"); unique(); stdout() }'

or within a script - see EXAMPLE SCRIPT

VARIABLES
=========
declaration:

${variable}="value"

concatenating strings and variables

${variable}="some_string${some_variable}"

Values are always quoted, every assigned value is treated as a string
i.e. this will result in an error:

${variable}=wrong

COMMANDLINE
===========
	${0} script name
	${1} is the first argument
	${n} n argument


GLOBALS
=======
turn on debugging:
	${debug}="true"


COMMENTS
========
leading '#'

FUNCTION SYNTAX
===============

	function()
	function("value", ...)
	function(${variable}, ...)

	functions with return variables:
	${variable}<-function("...", ...)

FUNCTIONS
=========

setcsvseparator(separator)
--------------------------
overides comma as default data separator, argument is a single character
i.e multiple characters as separators are not supported

readfile(filename)
--------------------------
reads data from file into memory.

read_match(filename, pattern)
--------------------------
reads lines that match pattern (casesensitive acting like grep ^pattern) from file into memory.
This is useful for huge input files, that need prior filtering for either saving
memory and time in subsequent processing.

command(command)
--------------------------
runs a command, writes errors to stderr. The commands output (stdout) is not written to memory!
If you need the output consider command2line().

${return_variable}<-command2line("command")
--------------------------
runs a command, writes only the first line into of output to the return variable.

	Example - store the output of the last command into vector:  
	${d}<-command2line(last)

	Example - a piped command chain:  
	${d}<-command2line(uname -a|awk '{ print $1 }')
	
	Example - single quotes have to be used within the date call:  
	${twoweeksago}<-command2line(date --date='14 days ago' +%s)

command2data(command)
--------------------------
overwrites the current data stored in memory. Use this if data needs to be read from command instead of a file.

stdout()
--------------------------
print the current data stored in memory to stdout.

order("column,column,...")
--------------------------
reorders the columns
data has 4 columns, we only keep 3rd and 1st and move them (3rd becomes first etc.).

	Example:
	order("3,1")

unique()
--------------------------
unique sort of the data / lines in memory

merge("column,column,...", "separator")
--------------------------
merges the specified and the next column with a separator. Blanks are invalid separators and will cause an error.

	Example:  merges the 1st and 2nd column and 4th and 5th with an underscore.
	merge("1,4", "_")

find_highest(column_key,column_value)
--------------------------
get the lines with the highest value of each key.

	Example: creates the search pattern (key) from column 1 and 2 and looks for the highest number in column 6 
	find_highest("1,2",6) 

	Example: creates the search pattern (key) from column 3 and looks for the highest number in column 8
	find_highest(3,8)

find_lowest(column_key,column_value)
--------------------------
get the lines with the lowest value of each key.
Example:  see find_highest()

sum(column_key,column_value)
--------------------------
sums the values of each key in specified columns. column_key and column_value should be numeric.

	Example:  
	sum(1,2)

sum_fs(column_key,column_value, "unit")
--------------------------
sums filesystem size with KB, MB, GB and TB in it.

	Example:
	sum_fs(1,2, "GB")


fs_usage("column_name,column_used,column_total", unit, precision)
--------------------------
Calculates usage in percent of given used and total size.
one example line of a csv file would look like this:
server1,55GB, 130TB
meaning server1 has used diskspace of 55 GB and a total space of 130 TB.

	Example: we use GB as base unit and a position after decimal point of 2
	fs_usage("1,2,3", GB, 2)

will return a disk usage of 0.04%:
server1,0.04

writefile(filename)
--------------------------
write data from memory to file

appendfile(filename)
--------------------------
appends data from memory to file

writedata(filename, variable)
--------------------------
write value of variable to file (overwrites file)

appenddata(filename, variable)
--------------------------
appends value of variable to file

numericalsort(column, order)
--------------------------
sorts numerical based on the number in <column>, order is either asc (ascending) or desc (descending)

remove(pattern, matchcase)
--------------------------
removes lines that match a certain pattern.
	
	Example: 
	remove("index:", true)

match(pattern, matchcase))
--------------------------
only matching lines are kept in the vector case insensitive.

	Example: 
	match("iotack.sys", true)

match_column(pattern, column, matchcase))
--------------------------
only lines are kept in the vector that match in a specified column case insensitive,
everything that matches "full" in the 7th column:
Example:  match("full", "7", false)

match_data(${data}, action)
--------------------------
simple comparison between two vectors, if the strings from the second vector matches
either keep ("match") or remove ("remove") the line from the first vector

	Example:
	match_data(${data}, "match")

remove_empty()
--------------------------
removes lines with empty value in any column

exit()
--------------------------
stops processing and exits

count()
--------------------------
counts lines in memory

count_data(${data})
--------------------------
counts the entries of the specified vector

timer_start()
--------------------------
start timer

timer_end()
--------------------------
end timer and show passed time (secs)

showvariables()
--------------------------
shows all defined variables

tail(n)
--------------------------
same as the tail, print the last n lines

head(n)
--------------------------
same as the head, print the first n lines

columns(n)
--------------------------
only keeps lines with certain number of columns.
Example: only lines with two columns are kept: columns("2")

math()
--------------------------
mathematical (numerical) comparisons, first arg is the column, the second the operation
operations:
- greater than n
- larger than n
- less than
- equals n

	Example:
	math("5", "larger than 4578")
	
	keeps only lines that have a numeric value larger than 4578 in the fifth column.

	${data}<-grep("pattern", "[true][false]")
	Like grep, matching lines are returned and are stored in a new vector.
	Second argument determines if search is case insensitive ("true") or case sensitive ("false").

	Example: 
	grep("p-app", true)

${data}<-grep_column("pattern", column, "[true][false]")
--------------------------
Like grep, searches only in a specified column. Matching lines are returned and stored in a new vector.
Third argument determines if search is case insensitive ("true") or case sensitive ("false").

	Example: lines that match "var" (case sensitive) in the second column are returned.
	math("var", 2, true) 

setdata(${data})
--------------------------
writes vector (${data}) to main memory that is used by functions. Overwrites the current memory.

EXAMPLE SCRIPT
==============

	#!/usr/local/bin/dp2

	readfile("data.csv")
	print("lines read:")
	count()
	match("app01")
	remove("full")
	order("1,2,5")
	unique()
	print ("processed lines:")
	count()
	writefile("processed.csv")


AUTHOR
Matthias Knoll

COPYRIGHT
	Copyright © Matthias Knoll.  License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
	This is free software: you are free to change and redistribute it.  There is NO WARRANTY, to the extent permitted by law.
	
	THIS SOFTWARE IS FOR EDUCATIONAL PURPOSES ONLY, USE AT YOUR OWN RISK, NO LIABILITY!

Mar 10, 2022


