#include "lib.h"
#include "parser.h"

// 0.4. 6.04.2018

using namespace lib;

namespace parser {
// convert fs size (B,..,TB) based on the units vector
// convert_index is the position in the units vector to which unit it should be
// converted
double convert_size(string s, vector<string> units, int convert_index) {

  transform(s.begin(), s.end(), s.begin(), ::tolower);
  size_t p, p1;
  int dotcnt = 0, start, found = 0, matchedunit_index;
  string unit, num = "";
  char c, size_separator = ' ';
  double size = -1;
  bool b = false, dbg = false;

  trim(s);

  // need to insert size_separator in case unit and size are not separated
  if (s.find(size_separator) == string::npos) {
    string o = "";
    for (auto i = 0; i < s.length(); i++) {
      c = s[i];
      if (isalpha(c) && !b) {
        o += size_separator;
        b = true;
      }
      o += c;
    }
    s = o;
  }

  vector<string> words = ssplit(s, size_separator);

  if (dbg) {
    cerr << "dbg - convert_size(): words vector:" << endl;
    vp(words);
  }

  for (auto i = 0; i < units.size(); i++) {
    unit = units[i];
    transform(unit.begin(), unit.end(), unit.begin(), ::tolower);
    for (auto j = 0; j < words.size(); j++) {
      trim(words[j]);
      if (words[j] == "") {
        if (dbg)
          cerr << "dbg - convert_size() skip empty vector element in words ("
               << j << ")\n";
        continue;
      }

      // one word
      if (words.size() == 1) {
        p1 = s.rfind(unit);
        if (p1 != string::npos) {
          p = p1;
          found++;
        }
        if (dbg)
          cerr << "dbg - convert_size() unit: " << unit << "|" << words[j]
               << " found:" << found << " " << p << endl;
      }

      if (unit == words[j]) {
        if (dbg)
          cerr << "dbg - convert_size() match unit: " << unit << "|" << words[j]
               << endl;
        matchedunit_index = i;
        if (!found)
          p = s.rfind(unit);
        found++;
      }
    }
  }

  if (found != 1)
    return size; // must find only one valid size measure

  s = s.substr(0, p);

  if (dbg)
    cerr << "dbg - convert_size() substr: " << s << " pos:" << p << endl;

  for (start = (p - 1); start >= 0; start--) {
    c = s[start];
    if (c == ' ' && num.length() == 0)
      continue;

    if (dbg)
      cerr << "dbg - convert_size():" << start << " " << c << endl;

    if (c == '.' || c == ',') {
      if (dotcnt == 0)
        num += c;
      dotcnt++;
      continue;
    }

    if (isalpha(c) || isspace(c))
      break;

    if (isdigit(c) || c == '.' || c == ',')
      num += c;
  }
  rtrim(num, ',');
  rtrim(num, '.');
  srev(num);

  parser::string_replace(num, ",", ".");
  size = s_todouble(num);

  // convert input to MB
  if (units[matchedunit_index] == "b") {
    size = (size / (1024 * 1024));
  }

  if (units[matchedunit_index] == "kb") {
    size = (size / 1024);
  }

  if (units[matchedunit_index] == "gb") {
    size = (size * 1024);
  }

  if (units[matchedunit_index] == "tb") {
    size = (size * (1024 * 1024));
  }

  if (dbg)
    cerr << "dbg - convert_size(): intermediate convert " << num << " "
         << units[matchedunit_index] << " to "
         << ":" << size << " MB" << endl;

  // convert to requested unit
  if (units[convert_index] == "b") {
    size = size * (1024 * 1024);
  }

  if (units[convert_index] == "kb") {
    size = (size * 1024);
  }

  if (units[convert_index] == "gb") {
    size = (size / 1024);
  }

  if (units[convert_index] == "tb") {
    size = (size / (1024 * 1024));
  }

  if (dbg)
    cerr << "dbg - convert_size(): converted: " << size << " "
         << units[convert_index] << endl;

  return size;
}

// removes whitespace but keeps them within quotes
void removewhite(string &s) {
  bool quoted = false;
  string str = "";
  char c;

  for (auto i = 0; i < s.length(); i++) {

    if (s[i] == '"' && !quoted) {
      quoted = true;
      str += s[i];
      continue;
    }

    if (s[i] == '"' && quoted) {
      quoted = false;
      str += s[i];
      continue;
    }

    if (isspace(s[i]) && !quoted)
      continue;

    str += s[i];
  }
  s = str;
}

// removes double quotes but not masked quotes
void removequotes(string &s) {
  bool quoted = false;
  string str = "";

  if (s[0] != '"' && s[0] != '\'')
    str += s[0];

  for (auto i = 1; i < s.length(); i++) {
    int pre = (i - 1);
    if ((s[i] == '"' || s[i] == '\'') && ! (s[pre] != '\\'))
      continue;
    str += s[i];
  }
  s = str;
}

// removes double quotes but not single quotes
void removedoublequotes(string &s) {
  bool quoted = false;
  string str = "";

  for (auto i = 0; i < s.length(); i++) {
    if (s[i] == '"')
      continue;
    str += s[i];
  }
  s = str;
}

bool valid_varname(string s) {
  string sub = s.substr(2, s.length() - 3);

  if (s[0] != '$' && s[1] != '{' && (s.at(s.length() - 1) != '}'))
    return false;

  for (auto i = 0; i < sub.length(); i++) {
    if (sub[i] == '_' || sub[i] == '-')
      continue;
    if (sub[i] == '$' || sub[i] == '{' || sub[i] == '}')
      return false;
    if (!isalnum(sub[i]))
      return false;
  }
  // cerr << " valid_varname()" << s << "|" << sub << endl;

  return true;
}

double mticks() {
  struct timeval tv;
  gettimeofday(&tv, 0);
  return (double)tv.tv_usec / 1000 + tv.tv_sec * 1000;
}

bool isfullpath(string s) {
  trim(s);
  if (s.empty())
    return false;
  sremove(s, '"');
  if (s[0] == '/')
    return true;

  return false;
}

bool isrelativpath(string s) {
  trim(s);
  if (s.empty())
    return false;
  sremove(s, '"');
  if (s.find("/") == string::npos || s[0] == '/')
    return false;

  return true;
}

bool string_replace(string &str, string from, string to) {
  size_t start_pos = 0;
  bool replaced = false;
  while ((start_pos = str.find(from, start_pos)) != string::npos) {
    str.replace(start_pos, from.length(), to);
    start_pos += to.length();
    replaced = true;
  }
  return replaced;
}

bool buildstring(vector<string> data, string &s, char delim) {
  bool invalid = true;
  s = "";
  for (auto j = 0; j < data.size(); j++) {
    s += data[j];
    if (j < data.size() - 1)
      s += delim;
  }

  if (data.size() == 0)
    return invalid;
	
return true;
}

bool buildstring(vector<int> p, vector<string> data, string &s, char delim) {
  bool invalid = false;
  s = "";
  for (auto j = 0; j < p.size(); j++) {
    if (p[j] >= data.size()) {
      cerr << "error buildstring(): position of out range (" << data.size()
           << "):" << p[j] << endl;
      invalid = true;
      break;
    }
    s += data[p[j]];
    if (j < p.size() - 1)
      s += delim;
  }
  return invalid;
}

// converts numbers to array indices (num-1 -> i.e. 2 will result in 1) and
// stores them into a vector<int> example: string2index(v, "1,2,5")
bool string2index(vector<int> &v, string arg) {
  vector<int> p;
  int pos;
  size_t cpos = arg.find(",");
  bool dbg = false;

  if (dbg)
    cerr << "string2index() arg:" << arg << " (" << cpos << ")" << endl;

  // get vector indices
  if (cpos != string::npos) {
    vector<string> a = ssplit(arg, ',');
    if (dbg)
      cerr << "string2index() a.size():" << a.size() << endl;
    for (int i = 0; i < a.size(); i++) {
      trim(a[i]);
      // some sanity checking
      if (a[i] == "") {
        continue;
      }

      if (atoi(a[i].c_str()) == 0) {
        cerr << "error: string2index() invalid non-numeric argument:" << arg
             << "\n";
        return false;
      }

      try {
        pos = stoi(a[i]);
      } catch (std::invalid_argument &e) {
        cerr << "error: string2index() invalid non-numeric argument:" << arg
             << "\n";
        return false;
      }
      p.push_back(pos - 1);
    }
  } else { // only one position
    try {
      pos = stoi(arg);
    } catch (std::invalid_argument &e) {
      cerr << "error: string2index() invalid non-numeric argument:" << arg
           << "\n";
      return false;
    }
    pos--;

    if (pos < 0) {
      cerr << "error: string2index(): invalid argument - out of range:" << arg
           << "\n";
      return false;
    }
    p.push_back(pos);
  }

  if (!p.size()) {
    cerr << "error - string2index() - empty vector with argument:" << arg
         << endl;
    return false;
  }

  v = p;
  return true;
}

bool stringvect2intvect(vector<int> &v, vector<string> a) {
  vector<int> p;
  int pos;

  // get vector indices
  for (int i = 0; i < a.size(); i++) {
    trim(a[i]);
    // some sanity checking
    if (a[i] == "") {
      continue;
    }

    if (atoi(a[i].c_str()) == 0) {
      cerr << "error: stringvect2intvect() invalid non-numeric argument:"
           << a[i] << "\n";
      return false;
    }

    try {
      pos = stoi(a[i]);
    } catch (std::invalid_argument &e) {
      cerr << "error: stringvect2intvect() invalid/non-numeric argument:"
           << a[i] << "\n";
      return false;
    }
    p.push_back(pos - 1);
  }

  if (!p.size()) {
    cerr << "error - stringvect2intvect() - empty vector with argument."
         << endl;
    return false;
  }
  v = p;
  return true;
}

string command2line(vector<string> vc) {
  // cerr << "command2line():function disabled!"<<endl;
  int n = 0;
  string s = "", cmd = "", output = "";
  char buffer[128];
  vector<string> lines;

  // build command string
  buildstring(vc, cmd, ' ');
  FILE *pipe = popen(cmd.c_str(), "r");
  if (!pipe) {
    cerr << "command2line(): could not open pipe for commmand " << cmd << endl;
    return output;
  }

  // cout << "command2line() - cmd:" << cmd << endl;
  while (!feof(pipe)) {
    if (fgets(buffer, 128, pipe) != NULL) {
      s = string(buffer);
      output += trim(s);
      if (!output.empty())
        lines.push_back(output);

      if (lines.size() == 1)
        break;
    }
  }
  pclose(pipe);
  // cout << "command2line(): lines.size()==" << lines.size()  << endl;

  if (lines.size() == 1) {
    return lines[0];
  }

  return s;
}

// match multiple patterns;
bool multiple_match(string s, vector<string> pattern) {
  int found = 0;
  for (auto i = 0; i < pattern.size(); i++) {
    if (s.find(pattern[i]) == string::npos)
      return false;
    found++;
    // cerr << "info multiple_match(): found " <<  pattern[i] << " in " << s <<
    // endl;
  }
  return (found == pattern.size());
}

// get data from command
bool command(vector<string> vc, string &e) {
  vector<string> v;
  int n = 0;
  string cmd = "";
  string errorfile = "/tmp/dp_err.tmp";

  // build command string
  buildstring(vc, cmd, ' ');
  cmd += " 2>" + errorfile;

  int rc = system(cmd.c_str());
  if (rc != 0) {
    cerr << "error command(): command " << cmd << " failed" << endl;
    vector<string> err = readfile(errorfile);
    vp_err(err);
    return false;
  }

  return true;
}

void vp(vector<string> v) {
  for (auto i = 0; i < v.size(); i++)
    cout << v[i] << endl;
}

void vp_err(vector<string> v) {
  for (auto i = 0; i < v.size(); i++)
    cerr << v[i] << endl;
}

void mp(map<string, string> m) {
  map<string, string>::iterator it;

  for (auto it = m.begin(); it != m.end(); it++)
    cout << it->first << ":" << it->second << endl;
}

void writedata(string file, string data) {
  fstream ofs;
  if (data.empty()) {
    cerr << "warn - writedata(): no data to write ... do nothing " << endl;
    return;
  }

  ofs.open(file.c_str(), fstream::out);

  if (!ofs) {
    cerr << "error - writedata(): cannot write " << file << endl;
    exit(1);
  }
  ofs << data << endl;
  ofs.close();
}

void appenddata(string file, string data) {
  ofstream ofs;
  ofs.open(file.c_str(), std::ios_base::app);

  if (data.empty()) {
    cerr << "warn - appenddata(): no data to write ... do nothing " << endl;
    return;
  }

  if (!ofs) {
    cerr << "error - appenddata(): cannot write " << file << endl;
    exit(1);
  }

  ofs << data << endl;
  ofs.close();
}

void appendfile(vector<string> v, string file) {
  ofstream ofs;
  ofs.open(file.c_str(), std::ios_base::app);

  if (v.size() == 0) {
    cerr << "warn - appendfile(): no data to write ... do nothing " << endl;
    return;
  }

  if (!ofs) {
    cerr << "error - appendfile(): cannot write " << file << endl;
    exit(1);
  }

  for (auto i = 0; i < v.size(); i++)
    ofs << v[i] << endl;

  ofs.close();
}

void writefile(vector<string> v, string file) {

  fstream ofs;

  if (v.size() == 0) {
    cerr << "warn - writefile(): no data to write ... do nothing " << endl;
    return;
  }

  ofs.open(file.c_str(), fstream::out);

  if (!ofs) {
    cerr << "error - writefile(): cannot write " << file << endl;
    exit(1);
  }

  for (auto i = 0; i < v.size(); i++)
    ofs << v[i] << endl;

  ofs.close();
}

vector<string> read_match(string f, string pattern) {
  string line;
  size_t p;
  vector<string> v;
  stringstream ss;

  ifstream file(f.c_str());
  trim(pattern);

  if (!file) {
    cerr << "error - read_match(): cannot read " << f << endl;
    exit(10);
  }

  if (pattern.empty()) {
    cerr << "error - read_match(): second argument empty or missing." << endl;
    exit(10);
  }

  while (getline(file, line)) {
    p = line.find(pattern);

    if (p == string::npos) {
      continue;
    }

    trim(line);
    if (line.length() == 0)
      continue; // empty lines
    v.push_back(line);
  }
  file.close();
  return v;
}

vector<string> readfile(string f) {
  string line;
  vector<string> v;
  stringstream ss;

  ifstream file(f.c_str());

  if (!file) {
    cerr << "error - readfile(): cannot read " << f << endl;
    exit(10);
  }

  while (getline(file, line)) {
    trim(line);
    if (line.length() == 0)
      continue; // empty lines
    v.push_back(line);
  }
  file.close();
  return v;
}

vector<string> readfile(string f, bool skip_empty) {
  string line;
  vector<string> v;
  stringstream ss;

  ifstream file(f.c_str());

  if (!file) {
    cerr << "error - readfile(): cannot read " << f << endl;
    exit(10);
  }

  while (getline(file, line)) {
    trim(line);
    if (line.length() == 0 && skip_empty)
      continue;
    v.push_back(line);
  }
  file.close();
  return v;
}

void tail(vector<string> v, string arg) {
  vector<int> n;
  trim(arg);
  if (arg.empty()) {
    cerr << "error - tail() - empty argument:" << arg << endl;
    exit(10);
    return;
  }

  if (!parser::string2index(n, arg)) {
    cerr << "error - tail() - wrong argument:" << arg << endl;
    exit(10);
    return;
  }

  int start = v.size() - 1;
  start -= n[0];
  if (start < 0)
    start = 0;
  for (auto i = start; i < v.size(); i++)
    cout << v[i] << endl;
}

void head(vector<string> v, string arg) {

  trim(arg);

  if (arg.empty()) {
    cerr << "error - head() - empty argument:" << arg << endl;
    return;
  }

  int n = s_toint(arg, true);

  if (n == -1) {
    cerr << "error - head() - wrong argument:" << arg << endl;
    return;
  }

  if (n >= v.size())
    n = v.size();

  for (auto i = 0; i < n; i++)
    cout << v[i] << endl;
}

void vec_uniquesort(vector<string> &vec) {
  sort(vec.begin(), vec.end());
  vec.erase(unique(vec.begin(), vec.end()), vec.end());
}

string epoch(vector<string> args) {

  cerr << "call epoch()\n";
  stringstream ss;
  ss.clear();
  ss.str("");

  time_t secs;
  time_t now;

  if (args[0].empty() || args.size() == 0) {
    cerr << "error epoch(): empty args\n";
    return "";
  }
  removequotes(args[0]);

  vector<string> parts = ssplit(args[0], ' ', true);
  int num = s_toint(parts[0]);

  if (parts.size() == 0) {
    cerr << "error epoch(): wrongs args:" << args[0] << "\n";
    return "";
  }

  if (parts[1] == "seconds")
    secs = num;
  if (parts[1] == "minutes")
    secs = (num * 60);
  if (parts[1] == "hours")
    secs = (num * (60 * 60));
  if (parts[1] == "days")
    secs = num * (60 * 60 * 24);
  if (parts[1] == "weeks")
    secs = num * (60 * 60 * 24 * 7);
  if (parts[1] == "month")
    secs = num * (60 * 60 * 24 * 7 * 12);

  time(&now);
  ss << (now - secs);

  cerr << "epoch(): " << ss.str() << "\n";

  return ss.str();
}

bool isquoted(string line) {
  char lc = line.at(line.length() - 1);
  if (line[0] == '"' && lc == '"')
    return true;
  return false;
}

bool iscomment(string line) {
  if (line[0] == '#')
    return true;
  return false;
}

vector<string> process_args(string s, bool &validcommand) {

  vector<string> v;
  char lc = s.at(s.length() - 1);
  validcommand = true;

  if (s[0] != '{' || lc != '}') {
    validcommand = false;
  }
  sremove(s, '{');
  sremove(s, '}');
  v = csvsplit(s, ';');

  if (v.size() == 0)
    validcommand = false;

  return v;
}

} // namespace parser