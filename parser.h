#include <iostream>
#include <string>
#include <map>
#include <sstream>
#include <ctime>
#include <iomanip>
#include <stdexcept>
#include <fstream>
#include <functional>
#include <cmath>
#include <chrono>
#include <algorithm>
#include <cctype>
#include <cstdio>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/param.h>
#include <dirent.h>
#include <errno.h>

#include "lib.h"

using namespace std;

extern char _binary_syntax_txt_start;
extern char _binary_syntax_txt_end;

namespace parser {

// evaluation functions
// this is called first to eval and set arguments passed on the commandline
bool eval_arguments(vector<string>a, vector<string> &d );

bool isvariable(string line);
bool isvariable_assign(string line);
bool isrvalue_function(string line);
bool isfunction(string line);
bool functionparts(string line);
void parser(string script, map<string,int>functiontable, vector<string> args);

void syntax();

/* evals variables and return a processed vector for function calls */
vector<string> setvariables(string f);
int evalvariables(string & line, map<string,string>var);
bool syntaxcheck(string f, vector<string>func_prototypes);
void setcsvseparator(char c);
void setcomment(char c);


// functions available in script
vector<string> readfile(string f);
vector<string> readfile(string f, bool skip_empty); // allows empty lines if needed
// only store lines that match, needed for huge input files where only a fraction is needed.
vector<string> read_match(string f, string pattern);
void order(vector<string> &v, vector<string> arg);
void sum_fs(vector<string> &v, string args, string column, string unit, string precision);
int fs_usage(vector<string> &v, string data_columns, string unit, string precision);
string epoch(vector<string>args);
void columns(vector<string> &v, string args, char delim);

void merge(vector<string> &v, string pair1, string pair2, char delim);
//void numericalsort(vector <string> &v, string args, string order);
void numericalsort_asc(vector <string> &v, string args);
void numericalsort_desc(vector <string> &v, string args);

void find_number_column(vector<string> &v, string pattern_column, string search_column, string type);
void math(vector <string> &v, string a, string b);
void match_column(vector<string> &v, string arg, string column, string ignorecase);

// functions that store / manage data
void setdata( vector<string> &v, string arg );
vector<string> grep(vector<string> &v, string arg, string action, string ignorecase);
vector<string> grep_column(vector<string> &v, string arg, string column, string ignorecase);

void setcolumnnames(vector<string>args);

// library functions in libfunctions.cpp
bool valid_varname(string s);
void removewhite(string &s);
void removequotes(string &s);
void removedoublequotes(string &s);
bool string_replace(string &str, string from, string to);
bool isrelativepath(string s);
bool isfullpath(string s);
bool string2index(vector<int>&v, string arg);
bool stringvect2intvect(vector<int>&v, vector<string> arg);
string extract_size(string s, vector<string>units, int &match);
double convert_size(string s, vector<string>units, int convert_index);
bool buildstring(vector<int>p, vector<string> data, string &s, char delim);
bool buildstring(vector<string> data, string &s, char delim);
bool multiple_match(string s, vector<string>pattern);
string command2line(vector<string>vc); // returns output as string
bool command(vector<string>vc, string & e); // run commands, store stderr
void vp(vector<string>v);
void vp_err(vector<string>v);
void writefile(vector<string>v, string file);
void writedata(string file, string data);
void appendfile(vector<string>v, string file);
void appenddata(string file, string data);
void append(vector<string>v, string data);
void insert(vector<string>v, string data);
void head(vector<string>v, string arg);
void tail(vector<string>v, string arg);
void vec_uniquesort(vector<string> &vec);
bool isquoted(string s);
void mp(map<string,string>m);
bool iscomment(string line);
vector<string> process_args(string s, bool &error);
}

